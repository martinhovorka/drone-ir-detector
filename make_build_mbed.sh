#!/bin/bash

# Mbed setup
readonly MBED_DEVICE_DEFAULT="NUCLEO_F446RE"
readonly MBED_TOOLCHAIN=GCC_ARM
MBED_DEVICE=""
MBED_MNTPOINT=""
MBED_TTY=""

readonly YELLOW='\033[1;33m'
readonly RED='\033[1;31m'
readonly GREEN='\033[1;32m'
readonly NC='\033[0m' # No Color

# build setup
readonly NAME=$(basename $0)
readonly BUILD_ROOT=$(dirname $(realpath $0))

readonly DIR_SRC=mbedos/irdetect
readonly DIR_SRC_USR=$DIR_SRC/src
readonly DIR_ETC=etc
readonly DIR_SBIN=sbin
readonly DIR_SUPPORT=support

# formatter config
readonly ASTYLE_CFG=$DIR_ETC/astyle/codestyle.cfg
readonly ASTYLE_BIN=$DIR_SBIN/astyle

# doxygen configuration
readonly DOXYGEN_CFG=$DIR_ETC/doxygen/doxyfile
readonly DOXYGEN_RESULT=$DIR_DOC/html

readonly THIS_SCRIPT="make_build_mbed.sh"
readonly LNK_BUILD="mbed_build"
readonly LNK_BUILD_FLASH="mbed_build_flash"
readonly LNK_BUILD_CLEAN="mbed_build_clean"
readonly LNK_BUILD_CLEAN_FLASH="mbed_build_clean_flash"
readonly LNK_BUILD_DOCS="build_docs"


detect_device()
{
    mbed detect 2>&1 | grep 'Detected ".*" connected to ".*" and using com port ".*"' &>/dev/null
    return $?
}

get_device()
{
    mbed detect 2>&1 | grep 'Detected ".*" connected to ".*" and using com port ".*"' | cut -d '"' -f 2
    return $?
}

get_mount_point()
{
    mbed detect 2>&1 | grep 'Detected ".*" connected to ".*" and using com port ".*"' | cut -d '"' -f 4
    return $?
}

get_port()
{
    mbed detect 2>&1 | grep 'Detected ".*" connected to ".*" and using com port ".*"' | cut -d '"' -f 6
    return $?
}

build_docs()
{
    [ -d $DOXYGEN_RESULT ] && rm -rf $DOXYGEN_RESULT
    [ -f $DOXYGEN_CFG ] && "error: unable to find doxygen configuration file" && exit 1
    doxygen $DOXYGEN_CFG
    return $?
}

detect_device
if [ $? -ne 0 ]
then
    echo "ERROR: Unable to detect device, using default: $MBED_DEVICE_DEFAULT"
    MBED_DEVICE=$MBED_DEVICE_DEFAULT
else
    MBED_DEVICE=$(get_device)
    [ $? -ne 0 ] && echo "ERROR: unable to get device" && exit 1

    MBED_MNTPOINT=$(get_mount_point)
    [ $? -ne 0 ] && echo "ERROR: unable to get mount point" && exit 1

    MBED_TTY=$(get_port)
    [ $? -ne 0 ] && echo "ERROR: unable to get serial port" && exit 1
fi

# build command setup
readonly CMD_BUILD="mbed compile --target $MBED_DEVICE --toolchain $MBED_TOOLCHAIN"
readonly CMD_BUILD_FLASH="$CMD_BUILD --flash --sterm"
readonly CMD_BUILD_CLEAN="$CMD_BUILD --clean"
readonly CMD_BUILD_CLEAN_FLASH="$CMD_BUILD_CLEAN --flash --sterm"
CMD_LAST=""

# just build sw
build()
{
    CMD_LAST="$CMD_BUILD"
    $CMD_BUILD
}

# build sw and flash device
build_flash()
{
    if [ -n "$MBED_MNTPOINT" -a -n "$MBED_TTY" ]
    then
        CMD_LAST="$CMD_BUILD_FLASH"
        $CMD_BUILD_FLASH
    else
        echo "WARNING: no physical device present; flashing the device is not possible...\n"
        CMD_LAST="$CMD_BUILD"
        $CMD_BUILD
    fi
}

# clean and build sw
build_clean()
{
    CMD_LAST="$CMD_BUILD_CLEAN"
    $CMD_BUILD_CLEAN
}

# clean and build sw then flash sw
build_clean_flash()
{
    if [ -n "$MBED_MNTPOINT" -a -n "$MBED_TTY" ]
    then
        CMD_LAST="$CMD_BUILD_CLEAN_FLASH"
        $CMD_BUILD_CLEAN_FLASH
    else
        echo "WARNING: no physical device present; flashing the device is not possible..."
        CMD_LAST="$CMD_BUILD_CLEAN"
        $CMD_BUILD_CLEAN
    fi
}

print_result()
{
    if [ $1 -eq 0 ]
    then
        echo -e "${YELLOW}
build command : $CMD_LAST
device .......: $MBED_DEVICE
mount point ..: $MBED_MNTPOINT
serial device : $MBED_TTY
${GREEN}
===============================================================================

                                    SUCCESS

===============================================================================
${NC}"
    else
    echo -e "${RED}
===============================================================================

                                    FAILURE

===============================================================================
${NC}"
    fi
}


# create directory for support binaries
if [ ! -d $DIR_SBIN ]
then
    mkdir $DIR_SBIN
    [ $? -ne 0 ] && "ERROR: failed to create directory '$DIR_SBIN'!" && exit 1
fi

# check if code formatter is installed; if not - build it
# do not build if configuration is not present
if [ ! -f $ASTYLE_BIN -a -f $ASTYLE_CFG ]
then
    pushd $DIR_SUPPORT
    ./build_astyle.sh $BUILD_ROOT/$DIR_SBIN
    [ $? -ne 0 ] && "ERROR: failed to build 'astyle'!" && exit 1
    popd
fi

# build code formatter if
if [ -f $ASTYLE_BIN -a -f $ASTYLE_CFG ]
then
    CMD="$(realpath $ASTYLE_BIN) --options=$(realpath $ASTYLE_CFG)"
    pushd $DIR_SRC_USR
    $CMD "*.cpp" "*.hpp"
    popd
else
    echo "ERROR: code formatter and/or its configuration is not installed !!!"
fi

# check for script name and run appropriate build method
if [ $NAME == $THIS_SCRIPT ]
then
    true &&
        ln -sf $THIS_SCRIPT $LNK_BUILD &&
        ln -sf $THIS_SCRIPT $LNK_BUILD_FLASH &&
        ln -sf $THIS_SCRIPT $LNK_BUILD_CLEAN &&
        ln -sf $THIS_SCRIPT $LNK_BUILD_CLEAN_FLASH &&
        ln -sf $THIS_SCRIPT $LNK_BUILD_DOCS &&
        exit 0
    [ $? -ne 0 ] && "error: failed to create build links" && exit 1
    exit 0

elif [ $NAME == $LNK_BUILD ]
then
    pushd $DIR_SRC
    build
    print_result $?
    popd

elif [ $NAME == $LNK_BUILD_FLASH ]
then
    pushd $DIR_SRC
    build_flash
    print_result $?
    popd
elif [ $NAME == $LNK_BUILD_CLEAN ]
then
    pushd $DIR_SRC
    build_clean
    print_result $?
    popd
elif [ $NAME == $LNK_BUILD_CLEAN_FLASH ]
then
    pushd $DIR_SRC
    build_clean_flash
    print_result $?
    popd
elif [ $NAME == $LNK_BUILD_DOCS ]
then
    build_docs
fi
