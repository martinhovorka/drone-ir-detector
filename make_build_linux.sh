#!/bin/bash

readonly NAME=$(basename $0)
readonly BUILD_ROOT=$(dirname $(realpath $0))

readonly YELLOW='\033[1;33m'
readonly RED='\033[1;31m'
readonly GREEN='\033[1;32m'
readonly NC='\033[0m' # No Color

# project structure
readonly PROJECT_ROOT_RELATIVE=".."
readonly DIR_BUILD=build
readonly DIR_BIN=bin
readonly DIR_SBIN=sbin
readonly DIR_SUPPORT=support
readonly DIR_ETC=etc
readonly DIR_SRC=linux
readonly DIR_OPT=opt
readonly DIR_DOC=doc

# code formatter
readonly ASTYLE_CFG=$DIR_ETC/cmake/codestyle.cfg
readonly ASTYLE_BIN=$DIR_SBIN/astyle

# doxygen configuration
readonly DOXYGEN_CFG=$DIR_ETC/doxygen/doxyfile
readonly DOXYGEN_RESULT=$DIR_DOC/html

# create shortcuts for build targets
readonly THIS_SCRIPT="make_build_linux.sh"
readonly LNK_DEBUG="linux_build_debug"
readonly LNK_RELEASE="linux_build_release"
readonly LNK_MINSIZEREL="linux_build_release_minimum_size"
readonly LNK_RELWITHDEBINFO="linux_build_release_debug_info"
readonly LNK_DOCS="build_docs"

###############################################################################
# common setup
###############################################################################

# create result bin dir
if [ ! -d $DIR_BIN ]
then
    mkdir $DIR_BIN
    [ $? -ne 0 ] && "error: failed to create directory '$DIR_BIN'!" && exit 1
fi

# create build directory
if [ ! -d $DIR_BUILD ]
then
    mkdir $DIR_BUILD
    [ $? -ne 0 ] && "error: failed to create directory '$DIR_BUILD'!" && exit 1
fi

# create directory for optional compoments
if [ ! -d $DIR_SBIN ]
then
    mkdir $DIR_SBIN
    [ $? -ne 0 ] && "error: failed to create directory '$DIR_SBIN'!" && exit 1
fi

# create directory for optional compoments
if [ ! -d $DIR_OPT ]
then
    mkdir $DIR_OPT
    [ $? -ne 0 ] && "error: failed to create directory '$DIR_OPT'!" && exit 1

    NEWDIR=$DIR_OPT/lib
    [ ! -d $NEWDIR ] && mkdir $NEWDIR
    [ $? -ne 0 ] && "error: failed to create directory '$NEWDIR'!" && exit 1

    NEWDIR=$DIR_OPT/include
    [ ! -d $NEWDIR ] && mkdir $NEWDIR
    [ $? -ne 0 ] && "error: failed to create directory '$NEWDIR'!" && exit 1
fi

# check if code formatter is installed; if not - build it
# do not build if configuration is not present
if [ ! -f $ASTYLE_BIN -a -f $ASTYLE_CFG ]
then
    pushd $DIR_SUPPORT
    ./build_astyle.sh $BUILD_ROOT/$DIR_SBIN
    [ $? -ne 0 ] && "error: failed to build 'astyle'!" && exit 1
    popd
fi

if [ ! -f $DIR_OPT/lib/libi2c.a ]
then
    pushd $DIR_SUPPORT
    ./build_libi2c.sh $BUILD_ROOT/$DIR_OPT
    [ $? -ne 0 ] && "error: failed to build 'libi2c'!" && exit 1
    popd
fi

if [ ! -d $DIR_OPT/include/boost ]
then
    pushd $DIR_SUPPORT
    ./build_boost.sh $BUILD_ROOT/$DIR_OPT/include
    [ $? -ne 0 ] && return $RC_FAILURE
    popd
fi

###############################################################################
# distinguish if base script is called or build link is invoked
###############################################################################

# create build target links if name of basescript is called
if [ $NAME == $THIS_SCRIPT ]
then

    true &&
        ln -sf $THIS_SCRIPT $LNK_DEBUG &&
        ln -sf $THIS_SCRIPT $LNK_RELEASE &&
        ln -sf $THIS_SCRIPT $LNK_MINSIZEREL &&
        ln -sf $THIS_SCRIPT $LNK_RELWITHDEBINFO &&
        ln -sf $THIS_SCRIPT $LNK_DOCS &&
        exit 0
    [ $? -ne 0 ] && "error: failed to create build links" && exit 1
    exit 0

elif [ $NAME == $LNK_DOCS ]
then
    [ -d $DOXYGEN_RESULT ] && rm -rf $DOXYGEN_RESULT
    [ -f $DOXYGEN_CFG ] && "error: unable to find doxygen configuration file" && exit 1
    doxygen $DOXYGEN_CFG
else # otherwise run build

    echo ">>> Formatting source core <<<"
    if [ -f $ASTYLE_BIN -a -f $ASTYLE_CFG ]
    then
        CMD="$(realpath $ASTYLE_BIN) --options=$(realpath $ASTYLE_CFG)"
        pushd $DIR_SRC
        $CMD "*.cpp" "*.hpp"
        popd
    else
        echo "error: code formatter and/or its configuration is not installed !!!"
    fi

    pushd $DIR_BUILD
    if [ "$1" == "-c" -o "$1" == "--clean" ]
    then
        CLEAN=1
    else
        CLEAN=0
    fi

    CMAKE_BUILD_TYPE=""
    BUILD=0

    if [ $NAME == $LNK_DEBUG ]
    then
        CMAKE_BUILD_TYPE="DEBUG"
        BUILD=1
    fi

    if [ $NAME == $LNK_RELEASE ]
    then
        CMAKE_BUILD_TYPE="RELEASE"
        BUILD=1
    fi

    if [ $NAME == $LNK_MINSIZEREL ]
    then
        CMAKE_BUILD_TYPE="MINSIZEREL"
        BUILD=1
    fi

    if [ $NAME == $LNK_RELWITHDEBINFO ]
    then
        CMAKE_BUILD_TYPE="RELWITHDEBINFO"
        BUILD=1
    fi

    echo ">>> Running cmake <<<"
    if [ -z "$CMAKE_BUILD_TYPE" ]
    then
        cmake $PROJECT_ROOT_RELATIVE
    else
        cmake $PROJECT_ROOT_RELATIVE -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    fi

    [ $? -ne 0 ] && "error: cmake preprocessing failed!" && exit 1

    if [ $CLEAN -eq 1 ]
    then
        echo ">>> Cleaning binary directory <<<"
        pushd $PROJECT_ROOT_RELATIVE/bin
        rm -f irdetect
        [ $? -ne 0 ] && "error: unable to clean build targets" && exit 1
        popd

        echo ">>> Cleaning build directory <<<"
        make clean
        [ $? -ne 0 ] && "error: unable to clean build directory" && exit 1
    fi

    if [ $BUILD -eq 1 ]
    then
        echo ">>> Building project <<<"
        make -j $(cat /proc/cpuinfo | grep processor | wc -l) install
        if [ $? -eq 0 ]
        then
            echo -e "${YELLOW}
install manifest:\n$(cat install_manifest.txt)${GREEN}
===============================================================================

                                    SUCCESS

===============================================================================
${NC}"
        else
            echo -e "${RED}
===============================================================================

                                    FAILURE

===============================================================================
${NC}"
        fi
    fi
    popd
fi
