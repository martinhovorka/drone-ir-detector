#include "mbed.h"
#include "platform/mbed_thread.h"

#include "MLX90641_I2C_Driver.h"
#include "MLX90641_API.h"
#include <cstdlib>
#include <cinttypes>

typedef enum
{
    i2cFreqMin = 100,
    i2cFreqStd = 400,
    i2cFreqMax = 1000,
} i2cFreq_t;

typedef enum
{
    // ROM ====================================================================
    addr_rom_min        = 0x0000,
    addr_rom_max        = 0x03FF,

    // RAM ====================================================================
    addr_ram_min        = 0x0400,
    addr_ram_max        = 0x05BF,

    // EEPROM =================================================================
    addr_ee_min         = 0x2400,
    addr_ee_ctrl1       = 0x240C, // Control Register 1
    addr_ee_ctrl2       = 0x240D, // Control Register 2
    addr_ee_i2ccnf      = 0x240E, // I2C Configuration Register
    addr_ee_mlxi2caddr  = 0x240F, // Melexis internal use (8 bit), I2C_Address (8bit)
    addr_ee_max         = 0x273F,

    // Registers (MLX reserved) ===============================================
    addr_mlx1_min       = 0x8000,
    addr_mlx1_max       = 0x800C,

    // Registers ==============================================================
    addr_reg_min        = 0x800D,
    addr_reg_ctrl1      = 0x800D, // Control Register 1
    addr_reg_ctrl2      = 0x800E, // Control Register 2
    addr_reg_i2ccnf     = 0x800F, // I2C Configuration Register
    addr_reg_mlxi2caddr = 0x8010, // Melexis internal use (8 bit), I2C_Address (8bit)
    addr_reg_max        = 0x8010,

    // Registers (MLX reserved) ===============================================
    addr_mlx2_min       = 0x8011,
    addr_mlx2_max       = 0x8016
} deviceAddress_t;

#define U16( _VAL_ ) ((uint16_t)( _VAL_ ))
#define U8( _VAL_ ) ((uint8_t)( _VAL_ ))

const uint8_t DEFAULT_I2C_ADDR = 0x33;

int irSensorInit(const int i2cFreq)
{
    if ((i2cFreqStd < i2cFreqMin) || (i2cFreqStd > i2cFreqMax))
    {
        return EXIT_FAILURE;
    }

    MLX90641_I2CInit();
    MLX90641_I2CFreqSet(i2cFreq);
    return EXIT_SUCCESS;
}

void heartbeat()
{
    static DigitalOut heartBeatLed(LED1);
    heartBeatLed = !heartBeatLed;
}

#define EEPROM_SIZE 832

static int rc(0);
static uint16_t reg_0x240F(0xFFFF);
static uint16_t eeMLX90641[EEPROM_SIZE];

void readAllAddresses()
{
    for (uint16_t addr(0x01); addr <= 0x7F; ++addr)
    {
        // print device address if detected
        reg_0x240F = U16(0xFFFF);
        rc = MLX90641_I2CRead(addr, U16(0x240F), U16(1), &reg_0x240F);

        if (rc == 0)
        {
            printf("addr = %x\n", reg_0x240F & U16(0xFF));
        }
    }
}

void readAllEeprom()
{
    memset(eeMLX90641, 0, EEPROM_SIZE * sizeof(uint16_t));
    rc = MLX90641_DumpEE(DEFAULT_I2C_ADDR, eeMLX90641);

    if (rc == 0)
    {
        printf("EEPROM read success\n");
    }
}

void readSingle(uint16_t addr = DEFAULT_I2C_ADDR)
{
    reg_0x240F = U16(0xFFFF);
    rc = MLX90641_I2CRead(addr, U16(0x240F), U16(1), &reg_0x240F);

    if (rc == 0)
    {
        printf("addr = %x\n", reg_0x240F & U16(0xFF));
    }
}

void withDriver()
{
    irSensorInit(i2cFreqStd);

    while (true)
    {
        heartbeat();
        readSingle();
        thread_sleep_for(100);
    }
}

void custom()
{
    //static const uint16_t address(U16(0x240F));
    I2C iic(I2C_SDA, I2C_SCL);
    const int addr7bit = DEFAULT_I2C_ADDR;      // 7 bit I2C address
    const int addr8bit = DEFAULT_I2C_ADDR << 1; // 8bit I2C address, 0x90
    char cmd[2];

    while (true)
    {
        printf("----\n");
        cmd[0] = U8(0x24);
        cmd[1] = U8(0x0F);
        rc = iic.write(addr8bit, cmd, 2);
        printf("\t%lld: rc = %d\n", time(0), rc);
        wait(0.5);
        //cmd[0] = 0x00;
        //rc = iic.write(addr8bit, cmd, 1);
        //printf("\t%lld: rc = %d\n", time(0), rc);
        cmd[0] = U8(0x00);
        cmd[1] = U8(0x00);
        rc = iic.read(addr8bit | U16(0x0001), cmd, 2);
        printf("\t%lld: rc = %d\n", time(0), rc);
        printf("\tresult = %d\n", (cmd[0] << 8) | cmd[1]);
        thread_sleep_for(1000);
    }
}

int main()
{
    custom();
    return 0;
}