#!/bin/bash

#readonly SSH_LOGIN="pi@192.168.1.250"
readonly SSH_LOGIN="pi@192.168.20.114"
readonly REMOTE_PATH="/home/pi/build/"
readonly SYNC_TARGETS="etc linux support make_build_linux.sh CMakeLists.txt"

# create shortcuts for build targets
readonly NAME=$(basename $0)
readonly THIS_SCRIPT="remote_build.sh"
readonly LNK_DEBUG="linux_remote_build_debug"
readonly LNK_RELEASE="linux_remote_build_release"
readonly LNK_MINSIZEREL="linux_remote_build_release_minimum_size"
readonly LNK_RELWITHDEBINFO="linux_remote_build_release_debug_info"

rsync -aczv $SYNC_TARGETS $SSH_LOGIN:$REMOTE_PATH
[ $? -ne 0 ] && echo "Error: failed to synchronize source code" && exit 1

BUILD_TYPE=""
if [ $NAME == $THIS_SCRIPT ]
then
    true &&
        ln -sf $THIS_SCRIPT $LNK_DEBUG &&
        ln -sf $THIS_SCRIPT $LNK_RELEASE &&
        ln -sf $THIS_SCRIPT $LNK_MINSIZEREL &&
        ln -sf $THIS_SCRIPT $LNK_RELWITHDEBINFO &&
        exit 0
    [ $? -ne 0 ] && "error: failed to create build links" && exit 1
    exit 0
elif [ $NAME == $LNK_DEBUG ]
then
    BUILD_TYPE="./linux_build_debug"
elif [ $NAME == $LNK_RELEASE ]
then
    BUILD_TYPE="./linux_build_release"
elif [ $NAME == $LNK_MINSIZEREL ]
then
    BUILD_TYPE="./linux_build_release_debug_info"
elif [ $NAME == $LNK_RELWITHDEBINFO ]
then
    BUILD_TYPE="./linux_build_release_minimum_size"
fi

ssh -C $SSH_LOGIN "pushd /home/pi/build; [ ! -e linux_build_debug ] && ./make_build_linux.sh; $BUILD_TYPE"
[ $? -ne 0 ] && echo "Error: remote build failed" && exit 1
