#!/bin/bash

#
# This script should prepare Debian based system for ARM MBED OS development
#

[ "$(whoami)" != "root" ] && echo "Error: script must be executed as root" && exit 1

# enable i386 architecture
dpkg --add-architecture i386
[ $? -ne 0 ] && echo "Error: Failed to add i386 architecture" && exit 1

# install system dependencies
aptitude -r install python2.7 python-pip git mercurial libc6:i386 libncurses5:i386 libstdc++6:i386
[ $? -ne 0 ] && echo "Error: Failed to install additional software" && exit 1

# install ARM MBED OS command line tools
pip install -U mbed-cli
[ $? -ne 0 ] && echo "Error: Unable to install MBED cli tools" && exit 1

# download and install GCC ARM Cortex-M/Cortex-R compiler
# https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm
readonly COMPILER_PKG=gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2
readonly COMPILER_URL=https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2019q4/RC2.1/${COMPILER_PKG}

# compiler installation setup
readonly COMPILER_LNK=gcc-arm # version agnostic link
readonly DIR_INSTALL=/opt # installation directory
readonly COMPILER_PATH=${DIR_INSTALL}/${COMPILER_LNK} # full compiler path
readonly COMPILER_PATH_BIN=${COMPILER_PATH}/bin # path to compiler binaries
readonly COMPILER_SYS_CFG=/etc/profile.d/gcc-arm-mbed-cfg.sh # global system configuration file

# switch to installation directory
pushd $DIR_INSTALL
[ $? -ne 0 ] && echo "Error: Unable to change directory to '$DIR_INSTALL'" && exit 1

# downmload compiler package
wget $COMPILER_URL
[ $? -ne 0 ] && echo "Error: Failed to download compiler package from '$COMPILER_URL'" && exit 1

# determine root directory within package
readonly COMPILER_DIR=$(tar -tjf $COMPILER_PKG | sort | head -1 | cut -d / -f 1)
[ -z "$COMPILER_DIR" ] && echo "Failed to determine compiler root directyory in package '$COMPILER_PKG'" && exit 1

# if same version already exists on system remove it
if [ -d $COMPILER_DIR ]
then
    echo "Warning: compiler directory '$COMPILER_DIR' already exists."
    rm -rf $COMPILER_DIR
    [ -z "$COMPILER_DIR" ] && echo "Failed to remove compiler root directory" && exit 1
fi

# extract compilert package
tar -xjf $COMPILER_PKG
[ $? -ne 0 ] && echo "Error: unable to extract compiler package '$COMPILER_PKG'" && exit 1

# create version agnostic link
ln -sf $COMPILER_DIR $COMPILER_LNK
[ $? -ne 0 ] && echo "Error: unable to create link to compiler root '$COMPILER_LNK -> $COMPILER_DIR'" && exit 1

# remove downloaded package
rm $COMPILER_PKG
[ $? -ne 0 ] && echo "Error: unable to remove compiler package '$COMPILER_PKG'" && exit 1

# set global configuration
mbed config -G GCC_ARM_PATH ${COMPILER_PATH_BIN}
[ $? -ne 0 ] && echo "Error: failed to se global configuration for MBED-CLI tools" && exit 1

# write generic configuration
echo -e "export MBED_GCC_ARM_PATH=${COMPILER_PATH_BIN}\nexport PATH=\$PATH:${COMPILER_PATH_BIN}" > $COMPILER_SYS_CFG
[ $? -ne 0 ] && echo "Error: failed to write global bash configuration to '$COMPILER_SYS_CFG'" && exit 1

popd
[ $? -ne 0 ] && echo "Error: failed to change back directory" && exit 1

echo "
/-----------------------------------------------------------------------------\\
|                                                                             |
|                                  SUCCESS                                    |
|                                                                             |
|Add following line \"source /etc/profile\" (if not present) to your ~/.bashrc  |
\-----------------------------------------------------------------------------/
"

exit 0
