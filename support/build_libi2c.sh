#!/bin//bash

#!/bin/bash
readonly INSTALL_DIR="$1"
readonly UNPACK_DIR=libi2c
readonly PACKAGE=${UNPACK_DIR}.tar.xz

true &&
    tar -xJf $PACKAGE &&
    pushd $UNPACK_DIR &&
    make -j &&
    cp libi2c.a "$INSTALL_DIR/lib" &&
    cp src/i2c.h "$INSTALL_DIR/include" &&
    popd &&
    rm -rf $UNPACK_DIR &&
    exit 0

exit 1