#!/bin/bash

#
# Script will test if OS is set up and compilation is working
#

readonly TEST_PROJECT=mbed-os-example-blinky
# readonly TEST_TARGET=NUCLEO_F411RE
readonly TEST_TARGET=NUCLEO_F446RE

echo "Testing project : $TEST_PROJECT"
echo "Testing target  : $TEST_TARGET"

if [ -d ${TEST_PROJECT} ]
then
    [ $? -ne 0 ] && echo "Warning: test project directory '$TEST_PROJECT' already exists" && exit 1
    rm -rf ${TEST_PROJECT}
    [ $? -ne 0 ] && echo "Error: to remove existing test project directory '$TEST_PROJECT'" && exit 1
fi

mbed import https://github.com/ARMmbed/${TEST_PROJECT}
[ $? -ne 0 ] && echo "Error: failed import test project" && exit 1

pushd $TEST_PROJECT
[ $? -ne 0 ] && echo "Error: failed to change directory to '$TEST_PROJECT'" && exit 1

mbed compile --target $TEST_TARGET --toolchain GCC_ARM --clean --flash
[ $? -ne 0 ] && echo "Error: failed to compile test project '$TEST_PROJECT' for target '$TEST_TARGET'" && exit 1

popd
[ $? -ne 0 ] && echo "Error: failed to change back directory" && exit 1

rm -rf ${TEST_PROJECT}
[ $? -ne 0 ] && echo "Error: to remove existing test project directory '$TEST_PROJECT'" && exit 1

echo "
/-----------------------------------------------------------------------------\\
|                                                                             |
|                                  SUCCESS                                    |
|                                                                             |
\-----------------------------------------------------------------------------/
"

exit 0
