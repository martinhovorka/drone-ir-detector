#!/bin/bash

#
# boards marked (+) are MBED OS fully supported boards that are supported by the emulator
#
#   Maple                LeafLab Arduino-style STM32 microcontroller board (r5)
# + NUCLEO-F103RB        ST Nucleo Development Board for STM32 F1 series - https://os.mbed.com/platforms/ST-Nucleo-F103RB/ (MBED 0S 2 only)
# + NUCLEO-F411RE        ST Nucleo Development Board for STM32 F4 series - https://os.mbed.com/platforms/ST-Nucleo-F411RE/ (MBED 0S 2, 5.4 - 5.15)
#   NetduinoGo           Netduino GoBus Development Board with STM32F4
#   NetduinoPlus2        Netduino Development Board with STM32F4
#   OLIMEXINO-STM32      Olimex Maple (Arduino-like) Development Board
#   STM32-E407           Olimex Development Board for STM32F407ZGT6
#   STM32-H103           Olimex Header Board for STM32F103RBT6
#   STM32-P103           Olimex Prototype Board for STM32F103RBT6
#   STM32-P107           Olimex Prototype Board for STM32F107VCT6
#   STM32F0-Discovery    ST Discovery kit for STM32F051 lines
#   STM32F4-Discovery    ST Discovery kit for STM32F407/417 lines
# + STM32F429I-Discovery ST Discovery kit for STM32F429/439 lines - https://os.mbed.com/platforms/ST-Discovery-F429ZI/ (MBED 0S 2, 5.4 - 5.15)
#   generic              Generic Cortex-M board; use -mcu to define the device
# 
# MCUS maked by (+) are supported by the emulator but board itself may not be
#   STM32F051R8
#   STM32F103RB
#   STM32F107VC
#   STM32F405RG
# + STM32F407VG ? GD32-F307VG - https://os.mbed.com/platforms/GD32-F307VG/ (MBED OS 5.10 - 5.15)
# + STM32F407ZG ? NUCLEO-F207ZG - https://os.mbed.com/platforms/ST-Nucleo-F207ZG/ (Pelion, MBED 0S 2, 5.4 - 5.15)
# + STM32F411RE ? MultiTech Dragonfly - https://os.mbed.com/platforms/MTS-Dragonfly/ (MBED 0S 2, 5.4 - 5.6)
# + STM32F429ZI ? NUCLEO-F429ZI - https://os.mbed.com/platforms/ST-Nucleo-F429ZI/ (Advanced. Pelion, MBED 0S 2, 5.4 - 5.15)

EMULATOR_VERSION=2.8.0-8
EMULATOR_PKG=xpack-qemu-arm-${EMULATOR_VERSION}-linux-x64.tgz
EMULATOR_PKG_PATH=xPacks/qemu-arm/${EMULATOR_VERSION}
EMULATOR_URL=https://github.com/xpack-dev-tools/qemu-arm-xpack/releases/download/v${EMULATOR_VERSION}/${EMULATOR_PKG}
EMULATOR_DIR="../bin/STMEmulator"

if [ -d $EMULATOR_DIR ]
then
    echo "Warning: emulator dir already exists - removing '$EMULATOR_DIR"
    rm -rf $EMULATOR_DIR
    [ $? -ne 0 ] && echo "Error: unable to remove emulator installation directory" && exit 1
fi

if [ ! -d $EMULATOR_DIR ]
then
    mkdir -p $EMULATOR_DIR
    [ $? -ne 0 ] && echo "Error: unable to create emulator installation directory" && exit 1
fi

pushd $EMULATOR_DIR
[ $? -ne 0 ] && echo "Error: unable to switch to directory '$EMULATOR_DIR'" && exit 1

wget $EMULATOR_URL
[ $? -ne 0 ] && echo "Error: unable to download emulator from '$EMULATOR_URL'" && exit 1

tar -xzf $EMULATOR_PKG
[ $? -ne 0 ] && echo "Error: unable to extract emulator package '$EMULATOR_PKG'" && exit 1

mv $EMULATOR_PKG_PATH/* .
[ $? -ne 0 ] && echo "Error: unable to move package content to '$(pwd)'" && exit 1

rm -rf $EMULATOR_PKG xPacks
[ $? -ne 0 ] && echo "Error: unable to remove installation package" && exit 1

popd
[ $? -ne 0 ] && echo "Error: unable to switchy back to working directory" && exit 1

echo "
/-----------------------------------------------------------------------------\\
|                                                                             |
|                                  SUCCESS                                    |
|                                                                             |
| STM emulator is now installed in: $EMULATOR_DIR/bin                    |
\-----------------------------------------------------------------------------/
"


# ./qemu-system-gnuarmeclipse -board NUCLEO-F411RE -cpu cortex-m4 -mcu STM32F411RE -serial /dev/ttyS0 -image ./mbed-os-test-tty.bin
