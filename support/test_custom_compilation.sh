#!/bin/bash

#
# Script will test if OS is set up and compilation is working on cuatom projects
#

CODE='
#include "mbed.h"
#include "platform/mbed_thread.h"
int main()
{
    DigitalOut led(LED1);
    bool ledState = false;
    while (true) {
        led = !led;
        ledState = !ledState;
        printf("%lld: Switching LED state! LED is now: %s\\n", time(NULL), (ledState == true) ? "ON" : "OFF");
        thread_sleep_for(500);
    }
}
'

readonly TEST_PROJECT=mbed-os-test-tty
# readonly TEST_TARGET=NUCLEO_F411RE
readonly TEST_TARGET=NUCLEO_F446RE
readonly TEST_CODE="${TEST_PROJECT}.cpp"

echo "Testing project : $TEST_PROJECT"
echo "Testing target  : $TEST_TARGET"

if [ -d ${TEST_PROJECT} ]
then
    [ $? -ne 0 ] && echo "Warning: test project directory '$TEST_PROJECT' already exists" && exit 1
    rm -rf ${TEST_PROJECT}
    [ $? -ne 0 ] && echo "Error: to remove existing test project directory '$TEST_PROJECT'" && exit 1
fi

mbed new ${TEST_PROJECT}
[ $? -ne 0 ] && echo "Error: failed create test project" && exit 1

pushd $TEST_PROJECT
[ $? -ne 0 ] && echo "Error: failed to change directory to '$TEST_PROJECT'" && exit 1

echo -e "$CODE" > $TEST_CODE
[ $? -ne 0 ] && echo "Error: failed to create source file '$TEST_PROJECT'" && exit 1

mbed compile --target $TEST_TARGET --toolchain GCC_ARM  --clean --flash
[ $? -ne 0 ] && echo "Error: failed to compile test project '$TEST_PROJECT' for target '$TEST_TARGET'" && exit 1

popd
[ $? -ne 0 ] && echo "Error: failed to change back directory" && exit 1

rm -rf ${TEST_PROJECT}
[ $? -ne 0 ] && echo "Error: to remove existing test project directory '$TEST_PROJECT'" && exit 1

echo "
/-----------------------------------------------------------------------------\\
|                                                                             |
|                                  SUCCESS                                    |
|                                                                             |
\-----------------------------------------------------------------------------/
"

exit 0
