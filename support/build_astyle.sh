#!/bin/bash
readonly INSTALL_DIR="$1"
readonly UNPACK_DIR=astyle
readonly PACKAGE=${UNPACK_DIR}_3-1.tar.xz

true &&
    tar -xJf $PACKAGE &&
    pushd $UNPACK_DIR &&
    cmake . &&
    make -j &&
    cp astyle "$INSTALL_DIR" &&
    popd &&
    rm -rf $UNPACK_DIR &&
    exit 0

exit 1