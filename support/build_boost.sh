#!/bin/bash

readonly INSTALL_DIR="$1"
readonly UNPACK_DIR=boost
readonly PACKAGE=${UNPACK_DIR}_1-72-0.tar.xz

true &&
    tar -xJf $PACKAGE &&
    mv $UNPACK_DIR $INSTALL_DIR &&
    exit 0

exit 1
