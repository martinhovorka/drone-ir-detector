project(${TARGET})

add_library(
    ${TARGET} SHARED
    ${TARGET_SRCS}
)

install(
    TARGETS ${TARGET}
    DESTINATION ${INSTALL_PREFIX}
)

target_include_directories(
    ${TARGET}
    PUBLIC
    .
)

if(DEFINED TARGET_LIBS)
    target_link_libraries(
        ${TARGET}
        ${TARGET_LIBS}
    )    
endif(DEFINED TARGET_LIBS)
