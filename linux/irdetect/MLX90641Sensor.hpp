/**
 * @file MLX90641Sensor.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-02-10
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef MLX90641_HPP
#define MLX90641_HPP

#include "MLX90641Api.hpp"
#include "Logger.hpp"
#include "IRDException.hpp"
#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>

#define MLX90641_FRAME_SIZE 242
#define MLX90641_EEPROM_SIZE 832
#define MLX90641_OBJECTTEMP_SIZE 192
#define MLX90641_DEFAULT_TA_SHIFT 5.0f
#define MLX90641_ROWS 12
#define MLX90641_COLS 16
#define MLX90641_EEADDR_LOW 0x2400
#define MLX90641_EEADDR_HIGH 0x2740

#define MLX90641_TRS_MIN -200.0f
#define MLX90641_TRS_MAX 200.0f

#define MLX90641_TAS_MIN -200.0f
#define MLX90641_TAS_MAX 200.0f

#define MLX90641_EMS_MIN 0.0f
#define MLX90641_EMS_MAX 1.0f

namespace seva
{
    namespace ird
    {
        class MLX90641Sensor : public MLX90641Api
        {
            public:

                typedef uint16_t address_t;
                static const address_t eepromAddrLow;
                static const address_t eepromAddrHigh;
                static const size_t sensorRows;
                static const size_t sensorCols;
                static const size_t frameSize;
                static const size_t eepromSize;
                static const size_t objectTempSize; // temperature
                static const float defaultAmbientTempShift;


                typedef uint16_t frameData_t[MLX90641_FRAME_SIZE];
                typedef uint16_t eeprom_t[MLX90641_EEPROM_SIZE];
                typedef float objectTemp_t[MLX90641_OBJECTTEMP_SIZE];

                typedef enum : uint8_t
                {
                    resError = 0xFF,
                    res16bit = 0x00,
                    res17bit = 0x01,
                    res18bit = 0x02,
                    res19bit = 0x03,
                } resolution_t;

                typedef enum : uint8_t
                {
                    rateError = 0xFF,
                    rate05Hz = 0x00, //  0.5Hz
                    rate1Hz = 0x01,
                    rate2Hz = 0x02,
                    rate4Hz = 0x03,
                    rate8Hz = 0x04,
                    rate16Hz = 0x05,
                    rate32Hz = 0x06,
                    rate64Hz = 0x07
                } refreshRate_t;

                typedef paramsMLX90641 parameters_t;

                struct DataPacket
                {
                    objectTemp_t pixTo; // object temperature
                    objectTemp_t pixTa; // object temperature ambient
                    float Tac; // temperature ambient calculated
                    float Tas; // temperature ambient shift
                    float Trc; // temperature reflected calculated
                    float Tr; // tempareture reflected
                    float ems; // object emmisivity
                    float Vdd; // supply voltage
                    resolution_t res; // resolution
                    refreshRate_t rrt; // refresh rate
                };

                /**
                 * @brief Construct a new MLX90641Sensor object
                 *
                 * @param device
                 * @param i2cAddr
                 */
                MLX90641Sensor(const std::string& device, const int setRetries = 10, const unsigned short i2cAddr = 0x33,
                               const resolution_t& resolution = res18bit, const refreshRate_t refreshRate = rate4Hz,
                               const float emissivity = 1.0f, const float reflectedTemp = 25.0f, const float ambientTempShift = MLX90641_DEFAULT_TA_SHIFT):
                    MLX90641Api(device, i2cAddr),
                    setRetries(setRetries),
                    emissivity(emissivity),
                    reflectedTemperature(reflectedTemp),
                    ambientTemperatureShift(ambientTempShift)
                {
                    LOG_TRACE;
                    int counter(setRetries);
                    LOG_MSG_INF("Setting sensor resolution");

                    while (getResolution() != resolution)
                    {
                        setResolution(resolution);
                        --counter;

                        if (counter < 0)
                        {
                            THROW_IRD_EXCEPTION("failed to set resolvalgrinution");
                        }
                    }

                    counter = setRetries;
                    LOG_MSG_INF("Setting sensor refresh rate");

                    while (getRefreshRate() != refreshRate)
                    {
                        setRefreshRate(refreshRate);
                        --counter;

                        if (counter < 0)
                        {
                            THROW_IRD_EXCEPTION("failed to set resolution");
                        }
                    }
                }

                /**
                 * @brief Destroy the MLX90641Sensor object
                 *
                 */
                virtual ~MLX90641Sensor()
                {
                    LOG_TRACE;
                }

                /**
                 * @brief Set the Resolution object
                 *
                 * @param resolution
                 * @return int
                 */
                int setResolution(const resolution_t& resolution)
                {
                    LOG_TRACE;
                    return MLX90641_SetResolution(resolution);
                }

                /**
                 * @brief Get the Resolution object
                 *
                 * @return resolution_t
                 */
                resolution_t getResolution()
                {
                    LOG_TRACE;
                    int rc(MLX90641_GetCurResolution());

                    if (rc < 0)
                    {
                        return resError;
                    }

                    return resolution_t(rc);
                }

                /**
                 * @brief Set the Refresh Rate object
                 *
                 * @param refreshRate
                 * @return int
                 */
                int setRefreshRate(const refreshRate_t& refreshRate)
                {
                    LOG_TRACE;
                    return MLX90641_SetRefreshRate(refreshRate);
                }

                /**
                 * @brief Get the Refresh Rate object
                 *
                 * @param refreshRate
                 * @return int
                 */
                refreshRate_t getRefreshRate()
                {
                    LOG_TRACE;
                    int rc(MLX90641_GetRefreshRate());

                    if (rc < 0)
                    {
                        return rateError;
                    }

                    return refreshRate_t(rc);
                }

                /**
                 * @brief Get the Sub Page Number object
                 *
                 * @param frameData
                 * @return int
                 */
                int getSubPageNumber(const frameData_t& frameData)
                {
                    LOG_TRACE;
                    return MLX90641_GetSubPageNumber(const_cast<frameData_t&>(frameData));
                }

                /**
                 * @brief Get the Emissivity object
                 *
                 * @return float
                 */
                float getEmissivity()
                {
                    LOG_TRACE;
                    eeprom_t eeprom;
                    parameters_t params;

                    if (dumpEE(eeprom) < 0)
                    {
                        THROW_IRD_EXCEPTION("unable to dump EEPROM");
                    }

                    if (extractParameters(eeprom, params) < 0)
                    {
                        THROW_IRD_EXCEPTION("unable to extract parameters");
                    }

                    return MLX90641_GetEmissivity(&params);
                }

                /**
                 * @brief
                 *
                 * @param eeprom
                 * @return int
                 */
                int dumpEE(eeprom_t& eeprom)
                {
                    LOG_TRACE;
                    memset(eeprom, 0x0, sizeof(eeprom_t));
                    return MLX90641_DumpEE(eeprom);
                }

                /**
                 * @brief
                 *
                 * @param eeprom
                 * @param params
                 * @return int
                 */
                int extractParameters(const eeprom_t& eeprom, parameters_t& params)
                {
                    LOG_TRACE;
                    memset(&params, 0x0, sizeof(parameters_t));
                    return MLX90641_ExtractParameters(const_cast<eeprom_t&>(eeprom), &params);
                }

                /**
                 * @brief Get the Frame Data object
                 *
                 * @param frame
                 * @return int subpage
                 */
                int getFrameData(frameData_t& frameData)
                {
                    LOG_TRACE;
                    memset(frameData, 0x0, sizeof(frameData_t));
                    int rc(MLX90641_GetFrameData(frameData));

                    if (rc < 0)
                    {
                        LOG_MSG_ERR("failed to get frame data");
                        return -1;
                    }

                    return 0;
                }

                /**
                 * @brief Get the Supply Voltage object (Vdd)
                 *
                 * @return float
                 */
                float getSupplyVoltage()
                {
                    eeprom_t eeprom;
                    frameData_t frameData;
                    parameters_t parameters;

                    if (dumpEE(eeprom) < 0)
                    {
                        THROW_IRD_EXCEPTION("failed to dump eeprom");
                    }

                    if (extractParameters(eeprom, parameters) < 0)
                    {
                        THROW_IRD_EXCEPTION("failed to get parameters");
                    }

                    if (getFrameData(frameData) < 0)
                    {
                        THROW_IRD_EXCEPTION("failed to get frame data");
                    }

                    return MLX90641_GetVdd(frameData, &parameters);
                }

                /**
                 * @brief Get the Ambient Temperature object (Ta)
                 *
                 * @return float
                 */
                float getAmbientTemperature()
                {
                    eeprom_t eeprom;
                    frameData_t frameData;
                    parameters_t parameters;

                    if (dumpEE(eeprom) < 0)
                    {
                        THROW_IRD_EXCEPTION("failed to dump eeprom");
                    }

                    if (extractParameters(eeprom, parameters) < 0)
                    {
                        THROW_IRD_EXCEPTION("failed to get parameters");
                    }

                    if (getFrameData(frameData) < 0)
                    {
                        THROW_IRD_EXCEPTION("failed to get frame data");
                    }

                    return MLX90641_GetTa(frameData, &parameters);
                }

                /**
                 * @brief Get the Object Temperature object (To)
                 *
                 * @param emisivity
                 * @param reflectedTemp
                 * @param objectTemp
                 * @return int
                 */
                int getObjectTemperature(const float emisivity, const float reflectedTemp, objectTemp_t objectTemp)
                {
                    eeprom_t eeprom;
                    frameData_t frameData;
                    parameters_t parameters;
                    memset(objectTemp, 0x0, sizeof(objectTemp_t));

                    if (dumpEE(eeprom) < 0)
                    {
                        LOG_MSG_ERR("unable to dump EEPROM data");
                        return -1;
                    }

                    if (extractParameters(eeprom, parameters) < 0)
                    {
                        LOG_MSG_ERR("unable to extract parameters");
                        return -2;
                    }

                    if (getFrameData(frameData) < 0)
                    {
                        LOG_MSG_ERR("unable to get frame data");
                        return -3;
                    }

                    MLX90641_CalculateTo(frameData, &parameters, emisivity, reflectedTemp, objectTemp);
                    return 0;
                }

                /**
                 * @brief Get the Object Temp object; use internal values
                 *
                 * @param objectTemp
                 * @return int
                 */
                int getObjectTemperature(objectTemp_t objectTemp)
                {
                    LOG_TRACE;
                    return getObjectTemperature(emissivity, reflectedTemperature, objectTemp);
                }

                /**
                 * @brief Get the Object Temp Shift object
                 *
                 * @param emissivity
                 * @param ambientTempShift
                 * @param objectTemp
                 * @return int
                 */
                int getObjectTemperatureAmbient(const float emissivity, const float ambientTempShift, objectTemp_t& objectTemp)
                {
                    eeprom_t eeprom;
                    frameData_t frameData;
                    parameters_t parameters;
                    memset(objectTemp, 0x0, sizeof(objectTemp));

                    if (dumpEE(eeprom) < 0)
                    {
                        LOG_MSG_ERR("unable to dump EEPROM data");
                        return -1;
                    }

                    if (extractParameters(eeprom, parameters) < 0)
                    {
                        LOG_MSG_ERR("unable to extract parameters");
                        return -2;
                    }

                    if (getFrameData(frameData) < 0)
                    {
                        LOG_MSG_ERR("unable to get frame data");
                        return -3;
                    }

                    const float reflectedTemp(MLX90641_GetTa(frameData, &parameters) - ambientTempShift);
                    MLX90641_CalculateTo(frameData, &parameters, emissivity, reflectedTemp, objectTemp);
                    return 0;
                }

                /**
                 * @brief Get the Object Temp Shift object; use internal values
                 *
                 * @return int
                 */
                int getObjectTemperatureAmbient(objectTemp_t& objectTemp)
                {
                    LOG_TRACE;
                    return getObjectTemperatureAmbient(emissivity, ambientTemperatureShift, objectTemp);
                }

                /**
                 * @brief
                 *
                 * @param objectTemp
                 */
                void printObjectTemperature(const objectTemp_t& objectTemp)
                {
                    const float* value(objectTemp);

                    for (size_t r(0); r < sensorRows; ++r)
                    {
                        for (size_t c(0); c < sensorCols; ++c)
                        {
                            std::clog << *value << ' ';
                            ++value;
                        }

                        std::clog << std::endl;
                    }
                }

                /**
                 * @brief
                 *
                 * @param sensor
                 * @param startAddr
                 * @param endAddr
                 */
                void printEEData(MLX90641Sensor& sensor, const uint16_t startAddr = MLX90641_EEADDR_LOW,  const uint16_t endAddr = MLX90641_EEADDR_HIGH)
                {
                    LOG_TRACE;
                    uint16_t eeMLX90641[MLX90641_EEPROM_SIZE];
                    memset(&eeMLX90641, 0x0, MLX90641_EEPROM_SIZE * sizeof(uint16_t));

                    if (((startAddr < MLX90641_EEADDR_LOW) ||
                         (startAddr > MLX90641_EEADDR_HIGH)) ||
                        ((endAddr < MLX90641_EEADDR_LOW) ||
                         (endAddr > MLX90641_EEADDR_HIGH)) ||
                        (endAddr < startAddr))
                    {
                        LOG_FMT_ERR("invalid start address or end address (startAddress = '%d' endAddress = '%d')", startAddr, endAddr);
                        return;
                    }

                    if (sensor.MLX90641_DumpEE(eeMLX90641))
                    {
                        LOG_MSG_ERR("failed to dump EEPROM");
                        return;
                    }

                    std::clog << "ADDR | OFF | VAL  |b 15  14  13  12  11  10   9   8 | 7   6   5   4   3   2   1   0  | FF00 00FF | DEC  " << std::endl;
                    std::clog << "-----+-----+------+---------------------------------+--------------------------------+-----------+------" << std::endl;

                    for (uint16_t addr(startAddr); addr <= endAddr; ++addr)
                    {
                        uint16_t offset(addr - MLX90641_EEADDR_LOW);
                        std::clog <<
                                  std::hex << std::uppercase << addr << " | " <<
                                  std::dec << std::setw(3) << std::setfill(' ') << std::right << offset << " | " <<
                                  std::hex << std::setw(4) << std::setfill('0') << std::hex << eeMLX90641[offset] << " |  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x8000) ? 1 : 0) << "  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x4000) ? 1 : 0) << "  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x2000) ? 1 : 0) << "  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x1000) ? 1 : 0) << "  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x0800) ? 1 : 0) << "  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x0400) ? 1 : 0) << "  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x0200) ? 1 : 0) << "  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x0100) ? 1 : 0) << " |" <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x0080) ? 1 : 0) << "  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x0040) ? 1 : 0) << "  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x0020) ? 1 : 0) << "  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x0010) ? 1 : 0) << "  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x0008) ? 1 : 0) << "  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x0004) ? 1 : 0) << "  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x0002) ? 1 : 0) << "  " <<
                                  std::setw(2) << std::setfill(' ') << ((eeMLX90641[offset] & 0x0001) ? 1 : 0) << "  | " <<
                                  std::hex << std::setw(4) << std::setfill('0') << (eeMLX90641[offset] & 0xFF00) << ' ' <<
                                  std::hex << std::setw(4) << std::setfill('0') << (eeMLX90641[offset] & 0x00FF) << " | " <<
                                  std::dec << std::setw(5) << std::setfill(' ') << std::left << eeMLX90641[offset]  << std ::endl;
                    }
                }

                /**
                 * @brief Set the Emissivity object
                 *
                 * @param e
                 */
                bool setEmissivity(const float e)
                {
                    if ((e <= MLX90641_EMS_MIN) || (e > MLX90641_EMS_MAX))
                    {
                        return false;
                    }

                    emissivity = e;
                    return true;
                }

                /**
                 * @brief Set the Reflected Temperature object
                 *
                 * @param rt
                 */
                bool setReflectedTemperature(const float trs)
                {
                    if ((trs < MLX90641_TRS_MIN) || (trs > MLX90641_TRS_MAX))
                    {
                        return false;
                    }

                    reflectedTemperature = trs;
                    return true;
                }

                /**
                 * @brief Set the Ambient Temperature object
                 *
                 * @param ta
                 */
                bool setAmbientTemperatureShift(const float tas)
                {
                    if ((tas < MLX90641_TAS_MIN) || (tas > MLX90641_TAS_MAX))
                    {
                        return false;
                    }

                    ambientTemperatureShift = tas;
                    return true;
                }

                /**
                 * @brief
                 *
                 * @param dp
                 * @param emissivity
                 * @param reflectedTemp
                 * @param ambientTempShift
                 * @return int
                 */
                int readDataPacket(DataPacket& dp, const float ems, const float Tr, const float Tas)
                {
                    LOG_TRACE;
                    //
                    eeprom_t eeprom;
                    frameData_t frameData;
                    parameters_t parameters;
                    memset(&dp, 0x0, sizeof(DataPacket));

                    if (dumpEE(eeprom) < 0)
                    {
                        LOG_MSG_ERR("failed to dump eeprom");
                        return -1;
                    }

                    if (extractParameters(eeprom, parameters) < 0)
                    {
                        LOG_MSG_ERR("failed to get parameters");
                        return -2;
                    }

                    if (getFrameData(frameData) < 0)
                    {
                        LOG_MSG_ERR("failed to get frame data");
                        return -3;
                    }

                    dp.ems = ems;
                    dp.Tas = Tas;
                    dp.Tr = Tr;
                    dp.Vdd = MLX90641_GetVdd(frameData, &parameters);
                    dp.Tac = MLX90641_GetTa(frameData, &parameters);
                    dp.Trc = dp.Tac - Tas;
                    MLX90641_CalculateTo(frameData, &parameters, ems, dp.Tr, dp.pixTo);
                    MLX90641_CalculateTo(frameData, &parameters, ems, dp.Trc, dp.pixTa);
                    dp.res = getResolution();
                    dp.rrt = getRefreshRate();
                    return 0;
                }

                /**
                 * @brief
                 *
                 * @param dp
                 * @return int
                 */
                int readDataPacket(DataPacket& dp)
                {
                    return readDataPacket(dp, emissivity, reflectedTemperature, ambientTemperatureShift);
                }

            private:

                int setRetries;
                float emissivity;
                float reflectedTemperature;
                float ambientTemperatureShift;
        };
    }
}
#endif