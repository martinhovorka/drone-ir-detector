/**
 * @file debugTools.cpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-02-17
 *
 * @copyright Copyright (c) 2020
 *
 */

#include "debugTools.hpp"
#include "Logger.hpp"

#include <iostream>
#include <iomanip>
#include <cstddef>

namespace seva
{
    namespace ird
    {
        #ifdef IRD_LOW_LEVEL_DBG
        void printBuffer(const void* buffer, const size_t length, const int type)
        {
            LOG_TRACE;

            if (buffer == nullptr)
            {
                LOG_MSG_ERR("bnull buffer pointer");
                return;
            }

            if (length <= 0)
            {
                LOG_MSG_ERR("invalid buffer length");
                return;
            }

            uint8_t* begin(((uint8_t*)(buffer)));
            uint8_t* end(begin + length);

            switch (type)
            {
                case DBG_BUF_TX:
                    std::clog << "<TX> ";
                    break;

                case DBG_BUF_RX:
                    std::clog << "<RX> ";
                    break;

                default:
                    std::clog << "<BUF> ";
                    break;
            }

            while (begin < end)
            {
                std::clog << std::setfill('0') << std::setw(2) << std::right << std::hex << int(*begin) << ' ';
                ++begin;
            }

            switch (type)
            {
                case DBG_BUF_TX:
                    std::clog << "</TX>" << std::endl;
                    break;

                case DBG_BUF_RX:
                    std::clog << "</RX>" << std::endl;
                    break;

                default:
                    std::clog << "</BUF>" << std::endl;
                    break;
            }
        }
        #endif
    }
}