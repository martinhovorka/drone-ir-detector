/**
 * @file IRDException.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-02-09
 *
 * @copyright Copyright (c) 2020
 *
 */


#ifndef IRDEXCEPTION_HPP
#define IRDEXCEPTION_HPP

#include <exception>
#include <string>
#include <ostream>
#include "Logger.hpp"

namespace seva
{
    namespace ird
    {
        class IRDException : public std::exception
        {
            public:
                /**
                 * @brief Construct a new IRDException object
                 *
                 * @param file file where exeception occure
                 * @param function function where exeception occure
                 * @param line line of exception throw
                 * @param message exception message
                 */
                IRDException(const std::string& file, const std::string& function, const int line, const std::string& message)
                    : file(file), function(function), line(line), message(message)
                {
                    LOG_TRACE;
                }

                friend std::ostream& operator<< (std::ostream& os, const IRDException& ex)
                {
                    LOG_TRACE;
                    os << "EXCEPTION @ " << ex.file << " : " << ex.line << " : " << ex.function << "() => "  << ex.message << std::endl;
                    return os;
                }

                /**
                 * @brief overridden from base exception
                 *
                 * @return const char* exception message
                 */
                virtual const char* what() const noexcept
                {
                    LOG_TRACE;
                    return message.c_str();
                }

                /**
                 * @brief Destroy the IRDException object
                 *
                 */
                virtual ~IRDException()
                {
                    LOG_TRACE;
                }

            private:
                std::string file;
                std::string function;
                int line;
                std::string message;
        };

#define THROW_IRD_EXCEPTION( _STR_ ) throw IRDException(__FILE__, __func__, __LINE__, _STR_);
    }
}

#endif
