/**
 * @file SensorMessage.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-02-28
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef SENSORMESSAGE_HPP
#define SENSORMESSAGE_HPP

#include "MLX90641Sensor.hpp"
#include "Logger.hpp"
#include <cinttypes>
#include <cstring>
#include <vector>
#include <sstream>

namespace seva
{
    namespace ird
    {
        class SensorMessage final
        {
            public:
                SensorMessage()
                {
                    memset(&data, 0x0, sizeof(data));
                }
                typedef enum : uint8_t
                {
                    msgStatusSuccess = 0x00, // request successful
                    msgStatusFailure = 0xFF, // request faild
                    msgReqVdd = 0x01, // req. - Sup[ply Voltage
                    msgRspVdd = 0x11, // rsp. - Sup[ply Voltage
                    msgReqTac = 0x02, // req. - Temperature - ambient calculated
                    msgRspTac = 0x12, // rsp. - Temperature - ambient calculated
                    msgReqPixTo = 0x03, // reg: all pixels - Reflected temperature is measured by user
                    msgRspPixTo = 0x13, // res: all pixels - Reflected temperature is measured by user
                    msgReqPixTa = 0x04, // reg: all pixels - Reflected temperature is based on Ambient temperature
                    msgRspPixTa = 0x14, // res: all pixels - Reflected temperature is based on Ambient temperature
                    msgSetTr = 0x21, // Temperature - reflected
                    msgSetEms = 0x22, // Emmisivity
                    msgSetTas = 0x23, // Temperature - ambient
                    msgSetRrt = 0x24, // Refresh Rate
                    msgSetRes = 0x25, // Resolution
                    msgReqAll = 0x0A, // request all data
                    msgRspAll = 0x1A // request all data
                } type_t;


                typedef union
                {
                    MLX90641Sensor::DataPacket dataPacket;
                    MLX90641Sensor::objectTemp_t pixTo;
                    MLX90641Sensor::objectTemp_t pixTa;
                    float Vdd;
                    float Tac;
                    float Tas;
                    float Tr;
                    float ems;
                    MLX90641Sensor::resolution_t res;
                    MLX90641Sensor::refreshRate_t rrt;
                } payload_t;

                typedef std::vector<uint8_t> buffer_t;

                /**
                 * @brief Get the Size object
                 *
                 * @return size_t
                 */
                size_t getSize(const type_t type) const
                {
                    LOG_TRACE;

                    switch (type)
                    {
                        case msgStatusSuccess:
                        case msgStatusFailure:
                        case msgReqVdd:
                        case msgReqTac:
                        case msgReqPixTo:
                        case msgReqPixTa:
                        case msgReqAll:
                            return sizeof(type_t);

                        case msgRspVdd:
                        case msgRspTac:
                        case msgSetTr:
                        case msgSetEms:
                        case msgSetTas:
                            return sizeof(type_t) + sizeof(float);

                        case msgRspPixTo:
                        case msgRspPixTa:
                            return sizeof(type_t) + sizeof(MLX90641Sensor::objectTemp_t);

                        case msgSetRrt:
                            return sizeof(type_t) + sizeof(MLX90641Sensor::refreshRate_t);

                        case msgSetRes:
                            return sizeof(type_t) + sizeof(MLX90641Sensor::resolution_t);

                        case msgRspAll:
                            return sizeof(type_t) + sizeof(MLX90641Sensor::DataPacket);
                    }

                    return 0;
                }

                typedef struct
                {
                    type_t type;
                    payload_t payload;
                } data_t;

                /**
                 * @brief Get the Size object
                 *
                 * @return size_t
                 */
                size_t getSize() const
                {
                    return getSize(data.type);
                }

                /**
                 * @brief Get the Data Ptr object
                 *
                 *
                 * @return void*
                 */
                data_t& getData()
                {
                    return data;
                }

                /**
                 * @brief Get the Max Size object
                 *
                 * @return size_t
                 */
                size_t getMaxSize() const
                {
                    return sizeof(data_t);
                }

                /**
                 * @brief Get the Type object
                 *
                 * @return type_t
                 */
                type_t getType() const
                {
                    return data.type;
                }

                /**
                 * @brief Set the Type object
                 *
                 * @param type
                 */
                void setType(type_t type)
                {
                    data.type = type;
                }

                /**
                 * @brief Get the As Buffer object
                 *
                 * @return std::vector<uint8_t>
                 */
                // buffer_t getAsBuffer() const
                // {
                //     LOG_TRACE;
                //     size_t size(data.type);
                //
                //    if (size == 0)
                //    {
                //        return buffer_t();
                //    }
                //
                //     buffer_t buffer(size);
                //     memcpy(buffer.data(), &data, size);
                //     return buffer;
                // }

                /**
                 * @brief Set the From Buffer object
                 *
                 * @param buffer
                 * @return true
                 * @return false
                 */
                // bool setFromBuffer(const buffer_t& buffer)
                // {
                //     LOG_TRACE;
                //
                //     if (buffer.empty())
                //     {
                //         return false;
                //     }
                //
                //     size_t size(static_cast<type_t>(buffer[0]));
                //
                //     if (size == 0)
                //     {
                //         return false;
                //     }
                //
                //     memcpy(&data, buffer.data(), size);
                //     return true;
                // }

                /**
                 * @brief return message as JSON
                 *
                 * @return std::string
                 */
                std::string toJson() const
                {
                    std::stringstream ss;

                    switch (data.type)
                    {
                        case msgRspAll:
                        {
                            ss << std::dec << "{\"pixTo\":[";
                            addobjTeperaturesToStream(ss, data.payload.dataPacket.pixTo);
                            ss << "],\"pixTa\":[";
                            addobjTeperaturesToStream(ss, data.payload.dataPacket.pixTa);
                            ss << "],\"Vdd\":" << data.payload.dataPacket.Vdd << ','
                               << "\"Tac\":" << data.payload.dataPacket.Tac << ','
                               << "\"Tas\":" << data.payload.dataPacket.Tas << ','
                               << "\"Trc\":" << data.payload.dataPacket.Trc << ','
                               << "\"Tr\":" << data.payload.dataPacket.Tr << ','
                               << "\"ems\":" << data.payload.dataPacket.ems << ','
                               << "\"rrt\":" << int(data.payload.dataPacket.rrt) << ','
                               << "\"res\":" << int(data.payload.dataPacket.res) << '}' << std::endl;
                        }
                        break;

                        case msgStatusSuccess:
                        {
                            ss << "{\"success\":true}" << std::endl;
                        }
                        break;

                        case msgStatusFailure:
                        {
                            ss << "{\"success\":false}" << std::endl;
                        }
                        break;

                        case msgRspVdd:
                        {
                            ss << "{\"Vdd\":" << std::dec << data.payload.Vdd << '}' << std::endl;
                        }
                        break;

                        case msgRspTac:
                        {
                            ss << "{\"Tac\":" << std::dec << data.payload.Tac << '}' << std::endl;
                        }
                        break;

                        case msgSetTr:
                        {
                            ss << "{\"req\":" << std::hex << int(data.type) << ",\"Tr\":" << std::dec << data.payload.Tr  << '}' << std::endl;
                        }
                        break;

                        case msgSetEms:
                        {
                            ss << "{\"req\":" << std::hex << int(data.type) << ",\"ems\":" << std::dec << data.payload.ems << '}' << std::endl;
                        }
                        break;

                        case msgSetTas:
                        {
                            ss << "{\"req\":" << std::hex << int(data.type) << ",\"Tas\":" << std::dec << data.payload.Tas  << '}' << std::endl;
                        }
                        break;

                        case msgRspPixTo:
                        {
                            ss << "{\"pixTo\":[";
                            addobjTeperaturesToStream(ss, data.payload.dataPacket.pixTo);
                            ss << "]}" << std::endl;
                        }
                        break;

                        case msgRspPixTa:
                        {
                            ss << "{\"pixTa\":[";
                            addobjTeperaturesToStream(ss, data.payload.dataPacket.pixTa);
                            ss << "]}" << std::endl;
                        }
                        break;

                        case msgSetRrt:
                        {
                            ss << "{\"req\":" << std::hex << int(data.type) << ",\"rrt\":" << std::hex << int(data.payload.rrt) << '}' << std::endl;
                        }
                        break;

                        case msgSetRes:
                        {
                            ss << "{\"req\":" << std::hex << int(data.type) << ",\"res\":" << std::hex << int(data.payload.res) << '}' << std::endl;
                        }
                        break;

                        case msgReqVdd:
                        case msgReqTac:
                        case msgReqPixTo:
                        case msgReqPixTa:
                        case msgReqAll:
                        {
                            ss << "{\"req\":" << std::hex << data.type << '}' << std::endl;
                        }
                        break;
                    }

                    return ss.str();
                }

            private:

                /**
                 * @brief add float array to JSON stream
                 *
                 * @param ss
                 */
                void addobjTeperaturesToStream(std::stringstream& ss, const MLX90641Sensor::objectTemp_t array) const
                {
                    LOG_TRACE;
                    int idx(0);
                    const int lim(MLX90641_OBJECTTEMP_SIZE - 1);

                    for (auto idx(0); idx < lim; ++idx)
                    {
                        ss << array[idx] << ',';
                    }

                    ss << array[idx];
                }

                data_t data;
        };
    }
}


#endif