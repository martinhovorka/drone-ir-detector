/**
 * @file MLX90641Sensor.cpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-02-17
 *
 * @copyright Copyright (c) 2020
 *
 */

#include "MLX90641Sensor.hpp"

namespace seva
{
    namespace ird
    {
        const MLX90641Sensor::address_t MLX90641Sensor::eepromAddrLow(MLX90641_EEADDR_LOW);
        const MLX90641Sensor::address_t MLX90641Sensor::eepromAddrHigh(MLX90641_EEADDR_HIGH);

        const size_t MLX90641Sensor::sensorRows(MLX90641_ROWS);
        const size_t MLX90641Sensor::sensorCols(MLX90641_COLS);
        const size_t MLX90641Sensor::frameSize(MLX90641_FRAME_SIZE);
        const size_t MLX90641Sensor::eepromSize(MLX90641_EEPROM_SIZE);
        const size_t MLX90641Sensor::objectTempSize(MLX90641_OBJECTTEMP_SIZE);
        const float MLX90641Sensor::defaultAmbientTempShift(MLX90641_DEFAULT_TA_SHIFT);
    }
}