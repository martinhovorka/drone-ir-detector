/**
 * @file AbstractI2CDevice.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-02-09
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef ABSTRACTI2CDEVICE_HPP
#define ABSTRACTI2CDEVICE_HPP

#include "i2c.h"
#include <cinttypes>
#include <string>
#include <cstring>
#include "Logger.hpp"
#include "IRDException.hpp"
#include "debugTools.hpp"

namespace seva
{
    namespace ird
    {
        class AbstractI2CDevice
        {
            public:
                /**
                 * @brief Construct a new Abstract I 2 C Device object
                 *
                 * @param device I2C device
                 * @param addr I2C device(slave) address
                 * @param tenbit I2C is 10 bit device address
                 * @param delay I2C internal operation delay, unit millisecond
                 * @param flags I2C i2c_ioctl_read/write flags
                 * @param page_bytes I2C max number of bytes per page, 1K/2K 8, 4K/8K/16K 16, 32K/64K 32 etc
                 * @param iaddr_bytes I2C device internal(word) address bytes, such as: 24C04 1 byte, 24C64 2 bytes
                 * @param ioctl whether to use ioctl API
                 *
                 */
                AbstractI2CDevice(
                    const std::string& device,
                    const unsigned short addr,
                    const unsigned char tenbit,
                    const unsigned char delay,
                    const unsigned short flags,
                    const unsigned int page_bytes,
                    const unsigned int iaddr_bytes,
                    const bool ioctl) :
                    device(device), busFd(-1), ioctl(ioctl)
                {
                    LOG_TRACE;
                    busFd = i2c_open(device.c_str());

                    if (busFd < 0)
                    {
                        LOG_FMT_FTL("unable to open device '%s'", device.c_str());
                        THROW_IRD_EXCEPTION("unable to device");
                    }

                    memset(&i2cDev, 0x0, sizeof(I2CDevice));
                    i2cDev.bus = busFd;
                    i2cDev.addr = addr;
                    i2cDev.tenbit = tenbit;
                    i2cDev.delay = delay;
                    i2cDev.flags = flags;
                    i2cDev.page_bytes = page_bytes;
                    i2cDev.iaddr_bytes = iaddr_bytes;
                }

                /**
                 * @brief Get the Slave Address object
                 *
                 * @return unsigned short
                 */
                unsigned short getSlaveAddress() const
                {
                    return i2cDev.addr;
                }

                /**
                 * @brief write data on I2C bus
                 *
                 * @param iaddr device memory address
                 * @param buffer data buffer to write to
                 * @param length buffer length
                 * @return ssize_t number of written bytes, negative value on error
                 */
                ssize_t I2CReadRaw(const unsigned iaddr, void* buffer, const size_t length)
                {
                    LOG_TRACE;
                    ssize_t bytesReturned;

                    if (buffer == nullptr)
                    {
                        LOG_FMT_ERR("null buffer pointer while reading from device '%s'", device.c_str());
                        return -1;
                    }

                    if (length <= 0)
                    {
                        LOG_FMT_ERR("invalid size requested while reading from device '%s'", device.c_str());
                        return -1;
                    }

                    if (ioctl)
                    {
                        bytesReturned = i2c_ioctl_read(&i2cDev, iaddr, buffer, length);
                    }
                    else
                    {
                        bytesReturned = i2c_read(&i2cDev, iaddr, buffer, length);
                    }

                    if (bytesReturned <= 0)
                    {
                        LOG_FMT_ERR("failed to read from device '%s'", device.c_str());
                        return -1;
                    }

                    if ((bytesReturned != (ssize_t)length))
                    {
                        LOG_FMT_WRN("read size mismatch, requested size ('%d') is not equal to returned size ('%d') for device '%s'", length, bytesReturned, device.c_str());
                    }

                    printBuffer(buffer, bytesReturned, DBG_BUF_RX);
                    return bytesReturned;
                }

                /**
                 * @brief read from I2C bus
                 *
                 * @param iaddr device memory address
                 * @param buffer data buffer to read from
                 * @param length
                 * @return ssize_t
                 */
                ssize_t I2CWriteRaw(const unsigned int iaddr, const void* buffer, const size_t length)
                {
                    LOG_TRACE;
                    ssize_t bytesReturned;

                    if (buffer == nullptr)
                    {
                        LOG_FMT_ERR("null buffer pointer while reading from device '%s'", device.c_str());
                        return -1;
                    }

                    if (length <= 0)
                    {
                        LOG_FMT_ERR("invalid size requested while reading from device '%s'", device.c_str());
                        return -1;
                    }

                    printBuffer(buffer, length, DBG_BUF_TX);

                    if (ioctl)
                    {
                        bytesReturned = i2c_ioctl_write(&i2cDev, iaddr, buffer, length);
                    }
                    else
                    {
                        bytesReturned = i2c_write(&i2cDev, iaddr, buffer, length);
                    }

                    if (bytesReturned <= 0)
                    {
                        LOG_FMT_ERR("failed to write to device '%s'", device.c_str());
                        return -1;
                    }

                    if ((bytesReturned != (ssize_t)length))
                    {
                        LOG_FMT_WRN("write size mismatch, requested size ('%d') is not equal to returned size ('%d') for device '%s'", length, bytesReturned, device.c_str());
                    }

                    return bytesReturned;
                }

                /**
                 * @brief Destroy the Abstract I 2 C Device object
                 *
                 */
                virtual ~AbstractI2CDevice()
                {
                    LOG_TRACE;

                    if (busFd > 0)
                    {
                        i2c_close(busFd);
                    }
                }

            protected:

                I2CDevice i2cDev;
                const std::string device;

            private:

                int busFd;
                const bool ioctl;
        };
    }
}

#endif