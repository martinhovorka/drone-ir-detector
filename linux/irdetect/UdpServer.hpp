/**
 * @file UdpServer.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-03-01
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef UDPSERVER_HPP
#define UDPSERVER_HPP

#include <thread>
#include "boost/asio.hpp"
#include "SensorMessage.hpp"
#include <vector>

namespace seva
{
    namespace ird
    {
        class UdpServer final
        {
            public:
                /**
                 * @brief Construct a new Udp Server object
                 *
                 * @param port
                 */
                UdpServer(const unsigned short port):
                    socket(io_context, boost::asio::ip::udp::endpoint(boost::asio::ip::udp::v4(), port))
                {
                }

                /**
                 * @brief
                 *
                 * @param message
                 * @return size_t
                 */
                size_t receive(SensorMessage& message)
                {
                    LOG_TRACE;
                    size_t returnedSize;
                    std::vector<uint8_t> buffer(sizeof(SensorMessage));
                    returnedSize = socket.receive_from(boost::asio::buffer(buffer, buffer.size()), sender_endpoint);
                    message.setType(static_cast<SensorMessage::type_t>(buffer[0]));
                    memcpy(&message.getData().payload, &buffer[1], message.getSize());
                    return returnedSize;
                }

                /**
                 * @brief
                 *
                 * @param message
                 * @return size_t
                 */
                //size_t send(SensorMessage& message)
                //{
                //    LOG_TRACE;
                //    std::vector<uint8_t> buffer(sizeof(SensorMessage));
                //    return socket.send_to(boost::asio::buffer(&message.getData(), message.getSize()), sender_endpoint);
                //}

                /**
                 * @brief
                 *
                 * @tparam T
                 * @param seqContainer
                 * @return size_t
                 */
                template <class T>
                size_t send(const T& seqContainer)
                {
                    LOG_TRACE;

                    if (seqContainer.size() == 0)
                    {
                        LOG_MSG_ERR("trying ty send empty packet");
                        return 0;
                    }

                    return socket.send_to(boost::asio::buffer(seqContainer, seqContainer.size()), sender_endpoint);
                }

            private:
                boost::asio::io_context io_context;
                boost::asio::ip::udp::socket socket;
                boost::asio::ip::udp::endpoint sender_endpoint;
        };
    }
}

#endif