/**
 * @file debugTools.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-02-17
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef DEBUGTOOLS_HPP
#define DEBUGTOOLS_HPP

#include <cinttypes>
#include <cstddef>

namespace seva
{
    namespace ird
    {

#define DBG_BUF_TX -1
#define DBG_BUF_RX 1
#define DBG_BUF_XX 0

#undef IRD_LOW_LEVEL_DBG

#ifdef IRD_LOW_LEVEL_DBG
    void printBuffer(const void* buffer, const size_t length, const int type);
#else
    #define printBuffer( _BUF_, _LEN_, _TYPE_ )
#endif

    }
}

#endif
