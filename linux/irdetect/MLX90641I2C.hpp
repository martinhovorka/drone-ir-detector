/**
 * @file MLX90641I2C.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-02-10
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef MLX90641I2C_HPP
#define MLX90641I2C_HPP

#include "AbstractI2CDevice.hpp"
#include "Logger.hpp"

namespace seva
{
    namespace ird
    {
        class MLX90641I2C : public AbstractI2CDevice
        {
            public:
                MLX90641I2C(const std::string& device, const unsigned short i2cAddr) :
                    AbstractI2CDevice(
                        device /*device*/,
                        i2cAddr /*addr*/,
                        0 /*tenbit*/,
                        1 /*delay*/,
                        0 /*flags*/,
                        16 /*page_bytes*/,
                        2 /*iaddr_bytes*/,
                        true /*ioctl*/)
                {
                    LOG_TRACE;
                }

                virtual ~MLX90641I2C()
                {
                    LOG_TRACE;
                }

            public:

                /**
                 * @brief MLX90641 I2C APPI read adaptor for Linux
                 *
                 * @param slaveAddr (just to ensure compatibility with API)
                 * @param startAddress device memory address on reading should start
                 * @param nWordsRead number of workds to read
                 * @param data data buffer to write to
                 * @return int -1 on failure 0 on success
                 */
                int MLX90641_I2CRead(uint8_t slaveAddr, uint16_t startAddress, uint16_t nWordsRead, uint16_t* data)
                {
                    LOG_TRACE;
                    //LOG_FMT_DBG("Started reading from device: '%x'", slaveAddr);

                    if (I2CReadRaw(startAddress, data, sizeof(uint16_t) * nWordsRead) < 0)
                    {
                        LOG_FMT_ERR("Failed to read from device: '%x'", slaveAddr);
                        return -1;
                    }
                    else
                    {
                        uint16_t* pData(data);
                        uint16_t* pDataEnd(&data[nWordsRead]);

                        while (pData < pDataEnd)
                        {
                            *pData = (((*pData & 0x00FF) << 8) | ((*pData & 0xFF00) >> 8));
                            ++pData;
                        }

                        return 0;
                    }
                }

                /**
                 * @brief MLX90641 I2C API write adaptor for Linux
                 *
                 * @param writeAddress
                 * @param data
                 * @return int
                 */
                int MLX90641_I2CWrite(const uint8_t slaveAddr, const uint16_t writeAddress, const uint16_t data)
                {
                    LOG_TRACE;
                    // LOG_FMT_DBG("Started writing to device: '%x', value: '%x'", slaveAddr, data);

                    if (I2CWriteRaw(writeAddress, &data, sizeof(uint16_t)) < 0)
                    {
                        LOG_FMT_ERR("Failed to write to device: '%x', value: '%x'", slaveAddr, data);
                        return -1;
                    }

                    uint16_t valueCheck;

                    if (I2CReadRaw(writeAddress, &valueCheck, sizeof(uint16_t)) != sizeof(uint16_t))
                    {
                        LOG_FMT_ERR("Failed to read written value from device: '%x', value: '%x'", slaveAddr, data);
                        return -2;
                    }

                    if (valueCheck != data)
                    {
                        // this is false pocitive issue; bit in register is used by Melexis firmware
                        if ((data == 0x30) && ((valueCheck == 0x0) || (valueCheck == 0x100)) && (writeAddress == 0x8000))
                        {
                            return 0;
                        }

                        LOG_FMT_ERR("Written value mismatch: device: '%x', requested: '%x', actual: '%x', address: '%x'", slaveAddr, data, valueCheck, writeAddress);
                        return -2;
                    }

                    return 0;
                }
        };
    }
}

#endif