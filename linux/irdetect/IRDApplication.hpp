/**
 * @file IRDApplication.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-02-09
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef IRDAPPLICATION_HPP
#define IRDAPPLICATION_HPP

#include <vector>
#include <string>
#include <unistd.h>
#include <iostream>
#include <chrono>
#include <thread>
#include <wiringPi.h>
#include "Logger.hpp"
#include "MLX90641Sensor.hpp"
#include "SensorMessage.hpp"
#include "UdpServer.hpp"


namespace seva
{
    namespace ird
    {
        class IRDApplication final
        {
            public:
                IRDApplication(const IRDApplication&) = delete;
                IRDApplication& operator=(const IRDApplication&) = delete;

                /**
                 * @brief Construct a new IRDApplication object
                 *
                 * @param argc
                 * @param argv
                 */
                IRDApplication(const int argc, const char* const* argv):
                    initSuccess(false), gpioLedId(7), server(nullptr)
                {
                    LOG_TRACE;

                    for (int i(0); i < argc; ++i)
                    {
                        arguments.push_back(std::string(argv[i]));
                    }

                    int option;

                    while ((option = getopt(argc, const_cast<char* const*>(argv), "h")) != -1)
                    {
                        switch (option)
                        {
                            case 'h':
                                printHelp();
                                return;

                            case ':':
                                LOG_FMT_FTL("option '-%c' needs an argument", optopt);
                                printHelp();
                                return;

                            case '?':
                                LOG_FMT_FTL("unknown option: %c", optopt);
                                printHelp();
                                return;
                        }
                    }

                    initSuccess = true;
                }

                /**
                 * @brief
                 *
                 * @return int
                 */
                int run()
                {
                    LOG_TRACE;

                    if (!initSuccess)
                    {
                        return EXIT_FAILURE;
                    }

                    int rc(0);
                    rc += irdInit();
                    rc += irdExec();
                    rc += irdExit();
                    return rc;
                }

                /**
                 * @brief Destroy the IRDApplication object
                 *
                 */
                ~IRDApplication()
                {
                    LOG_TRACE;

                    if (temperatureSensor != nullptr)
                    {
                        delete temperatureSensor;
                    }

                    if (server != nullptr)
                    {
                        delete server;
                    }
                }

            private:

                /**
                 * @brief
                 *
                 * @return int
                 */
                int irdInit()
                {
                    LOG_TRACE;
                    server = new UdpServer(9999);
                    temperatureSensor = new MLX90641Sensor(std::string("/dev/i2c-1"));

                    if (wiringPiSetup() != 0)
                    {
                        LOG_MSG_ERR("Faile to initialize Wiring PI library");
                        return 1;
                    }
                    else
                    {
                        pinMode(gpioLedId, OUTPUT);
                    }

                    return 0;
                }

                /**
                 * @brief
                 *
                 * @return int
                 */
                int irdExec()
                {
                    LOG_TRACE;
                    bool ledOn(true);

                    while (true)
                    {
                        if (ledOn)
                        {
                            digitalWrite(gpioLedId, HIGH);
                        }
                        else
                        {
                            digitalWrite(gpioLedId, LOW);
                        }

                        ledOn = !ledOn;

                        try
                        {
                            SensorMessage rx;
                            SensorMessage tx;
                            server->receive(rx);
                            LOG_FMT_DBG("received message id: '%x'", rx.getType());

                            switch (rx.getType())
                            {
                                case SensorMessage::msgReqVdd:
                                {
                                    tx.setType(SensorMessage::msgRspVdd);
                                    tx.getData().payload.Vdd = temperatureSensor->getSupplyVoltage();
                                }
                                break;

                                case SensorMessage::msgReqTac:
                                {
                                    tx.setType(SensorMessage::msgRspTac);
                                    tx.getData().payload.Tac = temperatureSensor->getAmbientTemperature();
                                }
                                break;

                                case SensorMessage::msgReqPixTo:
                                {
                                    tx.setType(SensorMessage::msgRspPixTo);
                                    temperatureSensor->getObjectTemperature(tx.getData().payload.pixTo);
                                }
                                break;

                                case SensorMessage::msgReqPixTa:
                                {
                                    tx.setType(SensorMessage::msgRspPixTa);
                                    temperatureSensor->getObjectTemperatureAmbient(tx.getData().payload.pixTa);
                                }
                                break;

                                case SensorMessage::msgSetTr:
                                {
                                    LOG_FMT_INF("Setting reflected temperature: '%f'", rx.getData().payload.Tr);

                                    if (temperatureSensor->setReflectedTemperature(rx.getData().payload.Tr))
                                    {
                                        tx.setType(SensorMessage::msgStatusSuccess);
                                    }
                                    else
                                    {
                                        tx.setType(SensorMessage::msgStatusFailure);
                                    }
                                }
                                break;

                                case SensorMessage::msgSetEms:
                                {
                                    LOG_FMT_INF("Setting emissivity: '%f'", rx.getData().payload.ems);

                                    if (temperatureSensor->setEmissivity(rx.getData().payload.ems))
                                    {
                                        tx.setType(SensorMessage::msgStatusSuccess);
                                    }
                                    else
                                    {
                                        tx.setType(SensorMessage::msgStatusFailure);
                                    }
                                }
                                break;

                                case SensorMessage::msgSetTas:
                                {
                                    LOG_FMT_INF("Setting ambient temperature shift: '%f'", rx.getData().payload.Tas);

                                    if (temperatureSensor->setAmbientTemperatureShift(rx.getData().payload.Tas))
                                    {
                                        tx.setType(SensorMessage::msgStatusSuccess);
                                    }
                                    else
                                    {
                                        tx.setType(SensorMessage::msgStatusFailure);
                                    }
                                }
                                break;

                                case SensorMessage::msgSetRrt:
                                {
                                    LOG_FMT_INF("Setting refresh rate: '%x'", rx.getData().payload.rrt);

                                    if (temperatureSensor->setRefreshRate(rx.getData().payload.rrt) == 0)
                                    {
                                        tx.setType(SensorMessage::msgStatusSuccess);
                                    }
                                    else
                                    {
                                        tx.setType(SensorMessage::msgStatusFailure);
                                    }
                                }
                                break;

                                case SensorMessage::msgSetRes:
                                {
                                    LOG_FMT_INF("Setting resolution: '%x'", rx.getData().payload.res);

                                    if (temperatureSensor->setResolution(rx.getData().payload.res) == 0)
                                    {
                                        tx.setType(SensorMessage::msgStatusSuccess);
                                    }
                                    else
                                    {
                                        tx.setType(SensorMessage::msgStatusFailure);
                                    }
                                }
                                break;

                                case SensorMessage::msgReqAll:
                                {
                                    tx.setType(SensorMessage::msgRspAll);
                                    temperatureSensor->readDataPacket(tx.getData().payload.dataPacket);
                                }
                                break;

                                case SensorMessage::msgStatusSuccess:
                                case SensorMessage::msgStatusFailure:
                                case SensorMessage::msgRspVdd:
                                case SensorMessage::msgRspTac:
                                case SensorMessage::msgRspPixTo:
                                case SensorMessage::msgRspPixTa:
                                case SensorMessage::msgRspAll:
                                default:
                                {
                                    tx.setType(SensorMessage::msgStatusFailure);
                                    LOG_FMT_ERR("Error invalid message type received: '%x'", rx.getType());
                                }
                                break;
                            }

                            LOG_FMT_DBG("sending message id: '%x'", tx.getType());
                            server->send(tx.toJson());
                        }
                        catch (std::exception& ex)
                        {
                            LOG_FMT_ERR("runtime exception: %s", ex.what());
                        }
                    }

                    return 0;
                }

                /**
                 * @brief
                 *
                 * @return int
                 */
                int irdExit()
                {
                    LOG_TRACE;
                    return 0;
                }

                /**
                 * @brief
                 *
                 */
                void printHelp() const
                {
                    LOG_TRACE;
                    std::clog
                            << *arguments.begin() << std::endl
                            << "\t-h : print help" << std::endl
                            << std::endl;
                }

                bool initSuccess;
                const int gpioLedId;
                std::vector<std::string> arguments;
                MLX90641Sensor* temperatureSensor;
                UdpServer* server;
        };
    }
}

#endif
