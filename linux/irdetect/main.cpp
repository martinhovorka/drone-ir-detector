/**
 * @file main.cpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-02-09
 *
 * @copyright Copyright (c) 2020
 *
 */

#include "Logger.hpp"
#include "IRDApplication.hpp"
#include <cstdlib>

void exitProcedure()
{
    LOG_TRACE;
    seva::logger::logDestroy_f();
}

int initProcedure()
{
    int rc(seva::logger::logInitialize_f(nullptr, seva::logger::logInf_e));
    LOG_TRACE;
    return rc;
}

int main(int argc, char* argv[])
{
    try
    {
        if (atexit(exitProcedure) != 0)
        {
            return EXIT_FAILURE;
        }

        if (initProcedure() != 0)
        {
            return EXIT_FAILURE;
        }

        LOG_TRACE;
        seva::ird::IRDApplication application(argc, argv);
        return application.run();
    }
    catch (const std::exception& e)
    {
        std::cerr << "Exception during application runtime:" <<  e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return 0;
}