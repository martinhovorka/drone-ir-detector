/**
 * @file Logger.hpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2020-01-21
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef LOGGER_H
#define LOGGER_H

#ifdef __cplusplus
    #include <cstdio>
#else
    #include <stdio.h>
#endif

#ifdef __cplusplus
namespace seva
{
    namespace logger
    {
#endif
        typedef enum
        {
            logDev_e = 0,
            logDbg_e,
            logInf_e,
            logWrn_e,
            logErr_e,
            logFtl_e,
        } logSeverityLevel_t;

        void logDestroy_f(void);
        int logInitialize_f(FILE* stream, const logSeverityLevel_t level);
        int logWriteMessage_f(const logSeverityLevel_t severity,
                              const int line,
                              const char* file,
                              const char* function,
                              const char* format,
                              ...);

#ifdef __cplusplus
    #define LOG_MSG_DEV(_MSG_) seva::logger::logWriteMessage_f(seva::logger::logDev_e, __LINE__, __FILE__, __func__, _MSG_)
    #define LOG_MSG_DBG(_MSG_) seva::logger::logWriteMessage_f(seva::logger::logDbg_e, __LINE__, __FILE__, __func__, _MSG_)
    #define LOG_MSG_INF(_MSG_) seva::logger::logWriteMessage_f(seva::logger::logInf_e, __LINE__, __FILE__, __func__, _MSG_)
    #define LOG_MSG_WRN(_MSG_) seva::logger::logWriteMessage_f(seva::logger::logWrn_e, __LINE__, __FILE__, __func__, _MSG_)
    #define LOG_MSG_ERR(_MSG_) seva::logger::logWriteMessage_f(seva::logger::logErr_e, __LINE__, __FILE__, __func__, _MSG_)
    #define LOG_MSG_FTL(_MSG_) seva::logger::logWriteMessage_f(seva::logger::logFtl_e, __LINE__, __FILE__, __func__, _MSG_)

    #define LOG_FMT_DEV(_MSG_, ...) seva::logger::logWriteMessage_f(seva::logger::logDev_e, __LINE__, __FILE__, __func__, _MSG_, ##__VA_ARGS__)
    #define LOG_FMT_DBG(_MSG_, ...) seva::logger::logWriteMessage_f(seva::logger::logDbg_e, __LINE__, __FILE__, __func__, _MSG_, ##__VA_ARGS__)
    #define LOG_FMT_INF(_MSG_, ...) seva::logger::logWriteMessage_f(seva::logger::logInf_e, __LINE__, __FILE__, __func__, _MSG_, ##__VA_ARGS__)
    #define LOG_FMT_WRN(_MSG_, ...) seva::logger::logWriteMessage_f(seva::logger::logWrn_e, __LINE__, __FILE__, __func__, _MSG_, ##__VA_ARGS__)
    #define LOG_FMT_ERR(_MSG_, ...) seva::logger::logWriteMessage_f(seva::logger::logErr_e, __LINE__, __FILE__, __func__, _MSG_, ##__VA_ARGS__)
    #define LOG_FMT_FTL(_MSG_, ...) seva::logger::logWriteMessage_f(seva::logger::logFtl_e, __LINE__, __FILE__, __func__, _MSG_, ##__VA_ARGS__)
#else
    #define LOG_MSG_DEV(_MSG_) logWriteMessage_f(logDev_e, __LINE__, __FILE__, __func__, _MSG_)
    #define LOG_MSG_DBG(_MSG_) logWriteMessage_f(logDbg_e, __LINE__, __FILE__, __func__, _MSG_)
    #define LOG_MSG_INF(_MSG_) logWriteMessage_f(logInf_e, __LINE__, __FILE__, __func__, _MSG_)
    #define LOG_MSG_WRN(_MSG_) logWriteMessage_f(logWrn_e, __LINE__, __FILE__, __func__, _MSG_)
    #define LOG_MSG_ERR(_MSG_) logWriteMessage_f(logErr_e, __LINE__, __FILE__, __func__, _MSG_)
    #define LOG_MSG_FTL(_MSG_) logWriteMessage_f(logFtl_e, __LINE__, __FILE__, __func__, _MSG_)

    #define LOG_FMT_DEV(_MSG_, ...) logWriteMessage_f(logDev_e, __LINE__, __FILE__, __func__, _MSG_, ##__VA_ARGS__)
    #define LOG_FMT_DBG(_MSG_, ...) logWriteMessage_f(logDbg_e, __LINE__, __FILE__, __func__, _MSG_, ##__VA_ARGS__)
    #define LOG_FMT_INF(_MSG_, ...) logWriteMessage_f(logInf_e, __LINE__, __FILE__, __func__, _MSG_, ##__VA_ARGS__)
    #define LOG_FMT_WRN(_MSG_, ...) logWriteMessage_f(logWrn_e, __LINE__, __FILE__, __func__, _MSG_, ##__VA_ARGS__)
    #define LOG_FMT_ERR(_MSG_, ...) logWriteMessage_f(logErr_e, __LINE__, __FILE__, __func__, _MSG_, ##__VA_ARGS__)
    #define LOG_FMT_FTL(_MSG_, ...) logWriteMessage_f(logFtl_e, __LINE__, __FILE__, __func__, _MSG_, ##__VA_ARGS__)
#endif

#if IRD_LOW_LEVEL_DBG
    #define LOG_TRACE LOG_MSG_DEV("trace")
#else
    #define LOG_TRACE
#endif

        #ifdef __cplusplus
    } // namespace logger
} // namespace seva
        #endif

#endif

