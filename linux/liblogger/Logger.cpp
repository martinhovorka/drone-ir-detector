#include "Logger.hpp"

#ifdef __cplusplus
    #include <cstdlib>
    #include <cstdarg>
    #include <cstdio>
    #include <cstring>
#else
    #include <stdlib.h>
    #include <stdarg.h>
    #include <stdio.h>
    #include <string.h>
#endif

#include <pthread.h>
#include <sys/syscall.h>
#include <unistd.h>

#ifdef __cplusplus
namespace seva
{
    namespace logger
    {
#endif
        typedef enum
        {
            elFalse_e,
            elTrue_e,
        } elBool_t;

        typedef struct
        {
            elBool_t isInitialized;
            pthread_mutex_t streamLock;
            logSeverityLevel_t level;
            FILE* stream;
        } logData_t;

        static logData_t loggerData = {elFalse_e, PTHREAD_MUTEX_INITIALIZER, logInf_e, NULL};

        void logDestroy_f(void)
        {
            if (loggerData.isInitialized != elTrue_e)
            {
                return;
            }

            if (pthread_mutex_lock(&loggerData.streamLock) != 0)
            {
                perror("FTL error during locking file stream mutex during exit");
                return;
            }

            if (loggerData.stream != NULL)
            {
                if (fflush(loggerData.stream) != 0)
                {
                    perror("FTL error during flushing file stream during exit");
                    return;
                }

                if (fclose(loggerData.stream) != 0)
                {
                    perror("FTL error during closing log stream");
                    return;
                }
            }

            if (pthread_mutex_unlock(&loggerData.streamLock) != 0)
            {
                perror("FTL error during unlocking file stream mutex during exit");
                return;
            }

            if (pthread_mutex_destroy(&loggerData.streamLock))
            {
                perror("FTL unable to destroy file stream mutex during exit");
                return;
            }

            loggerData.isInitialized = elFalse_e;
        }

        int logInitialize_f(FILE* stream, const logSeverityLevel_t level)
        {
            if (stream == NULL)
            {
                loggerData.stream = stdout;
            }
            else
            {
                loggerData.stream = stream;
            }

            loggerData.level = level;

            if (pthread_mutex_init(&loggerData.streamLock, NULL) != 0)
            {
                perror("FTL unable to initialize file stream mutex");
                return EXIT_FAILURE;
            }

            loggerData.isInitialized = elTrue_e;
            return EXIT_SUCCESS;
        }

        int logWriteMessage_f(const logSeverityLevel_t severity,
                              const int line,
                              const char* file,
                              const char* function,
                              const char* format,
                              ...)
        {
            if (loggerData.isInitialized != elTrue_e)
            {
                return EXIT_FAILURE;
            }

            if (severity < loggerData.level)
            {
                return EXIT_FAILURE;
            }

            int rc = EXIT_SUCCESS;
            char timeString[20] =
            {
                'Y', 'Y', 'Y', 'Y', '-', 'M', 'M', '-', 'D', 'D', '_',
                'H', 'H', ':', 'M', 'M', ':', 'S', 'S', '\0'
            };
            time_t rawtime = time(NULL);

            if (strftime(timeString, 20, "%Y-%m-%d_%H:%M:%S", localtime(&rawtime)) == 0)
            {
                timeString[0] = 'Y';
                timeString[1] = 'Y';
                timeString[2] = 'Y';
                timeString[3] = 'Y';
                timeString[4] = '-';
                timeString[5] = 'M';
                timeString[6] = 'M';
                timeString[7] = '-';
                timeString[8] = 'D';
                timeString[9] = 'D';
                timeString[10] = '_';
                timeString[11] = 'H';
                timeString[12] = 'H';
                timeString[13] = ':';
                timeString[14] = 'M';
                timeString[15] = 'M';
                timeString[16] = ':';
                timeString[17] = 'S';
                timeString[18] = 'S';
                timeString[19] = '\0';
            }

            if (pthread_mutex_lock(&loggerData.streamLock) != 0)
            {
                perror("FTL unable to lock file stream mutex");
                return EXIT_FAILURE;
            }

            switch (severity)
            {
                case logDev_e:
                    rc = fprintf(loggerData.stream, "DEV %s ", timeString);
                    break;

                case logDbg_e:
                    rc = fprintf(loggerData.stream, "DBG %s ", timeString);
                    break;

                case logInf_e:
                    rc = fprintf(loggerData.stream, "INF %s ", timeString);
                    break;

                case logWrn_e:
                    rc = fprintf(loggerData.stream, "WRN %s ", timeString);
                    break;

                case logErr_e:
                    rc = fprintf(loggerData.stream, "ERR %s ", timeString);
                    break;

                case logFtl_e:
                    rc = fprintf(loggerData.stream, "FTL %s ", timeString);
                    break;
            }

            if (rc < 0)
            {
                perror("FTL error during writing log message header");
                rc = EXIT_FAILURE;
            }

            va_list arguments;
            va_start(arguments, format);

            if (vfprintf(loggerData.stream, format, arguments) < 0)
            {
                perror("FTL error during writing log message");
                rc = EXIT_FAILURE;
            }

            va_end(arguments);
            const char* trimmedFile = file;

            if (trimmedFile != NULL)
            {
                trimmedFile = strrchr(trimmedFile, '/');

                if (trimmedFile == NULL)
                {
                    trimmedFile = file;
                }
                else
                {
                    if (strlen(trimmedFile) != 1)
                    {
                        trimmedFile++;
                    }
                }
            }

            if (fprintf(loggerData.stream, " [%u/%u/%ld %s:%d:%s()]\n", getppid(), getpid(), syscall(SYS_gettid), trimmedFile, line, function) < 0)
            {
                perror("FTL error during writing log message trace");
                rc = EXIT_FAILURE;
            }

            if (fflush(loggerData.stream) != 0)
            {
                perror("FTL error during flushing file stream");
                rc = EXIT_FAILURE;
            }

            if (pthread_mutex_unlock(&loggerData.streamLock) != 0)
            {
                perror("FTL unable to unlock file stream mutex");
                return EXIT_FAILURE;
            }

            return rc;
        }
        #ifdef __cplusplus
    } // namespace logger
} // namespace seva
        #endif