#!/usr/bin/python3

"""
main program module
"""

import socket
import json
import threading
import pprint
import struct

import PyQt5.QtWidgets as Qt5Widgets
import PyQt5.QtCore as Qt5Core
import PyQt5.QtGui as Qt5Gui


class UdpClient(threading.Thread):
    """
    udp client base class
    """

    def __init__(self, host, port):
        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.__endpoint = (host, port)
        self.__run = True
        self.__lock = threading.Lock()
        threading.Thread.__init__(self)

    def stop(self):
        """
        stop thread execution
        """
        self.__run = False

    def run(self):
        """
        thread body
        """
        while self.__run:
            self.__lock.acquire()
            self.__socket.sendto(b"\x0A", self.__endpoint)
            data, _ = self.__socket.recvfrom(65536)
            self.__lock.release()

            sensor_data = json.loads(data.decode("utf-8") )

            regenerate_table(UPDATED_WIDGETS[Constants.WIDGET_TEMPTABLE],
                             sensor_data[Constants.WIDGET_TEMPTABLE])

            regenerate_table(UPDATED_WIDGETS[Constants.WIDGET_TEMPTABLEAMBSHIFTED],
                             sensor_data[Constants.WIDGET_TEMPTABLEAMBSHIFTED])

            UPDATED_WIDGETS[Constants.WIDGET_VDD].\
                setText(str(sensor_data[Constants.WIDGET_VDD]))
            UPDATED_WIDGETS[Constants.WIDGET_TAMBCALC].\
                setText(str(sensor_data[Constants.WIDGET_TAMBCALC]))
            UPDATED_WIDGETS[Constants.WIDGET_REFLECTEDTEMP].\
                setText(str(sensor_data[Constants.WIDGET_REFLECTEDTEMP]))
            UPDATED_WIDGETS[Constants.WIDGET_EMISSIVITY].\
                setText(str(sensor_data[Constants.WIDGET_EMISSIVITY]))
            UPDATED_WIDGETS[Constants.WIDGET_AMBIENTTEMPSHIFT].\
                setText(str(sensor_data[Constants.WIDGET_AMBIENTTEMPSHIFT]))
            UPDATED_WIDGETS[Constants.WIDGET_REFLECTEDTEMPCALC].\
                setText(str(sensor_data[Constants.WIDGET_REFLECTEDTEMPCALC]))

            for key, val in Constants.RESOLUTIONS.items():
                if val == sensor_data[Constants.WIDGET_RESOLUTION]:
                    UPDATED_WIDGETS[Constants.WIDGET_RESOLUTION].setText(key)
                    break

            for key, val in Constants.REFRESH_RATES.items():
                if val == sensor_data[Constants.WIDGET_REFRESHRATE]:
                    UPDATED_WIDGETS[Constants.WIDGET_REFRESHRATE].setText(key)
                    break

            sensor_data[Constants.WIDGET_TEMPTABLE] = ["..."]
            sensor_data[Constants.WIDGET_TEMPTABLEAMBSHIFTED] = ["..."]
            #pprint.pprint(sensor_data)

    def make_alert(self, message_type, value):
        """
        test asyn method
        """
        self.__lock.acquire()
        self.__socket.sendto(message_type + value, self.__endpoint)
        data, _ = self.__socket.recvfrom(65536)
        self.__lock.release()
        pprint.pprint(data)

    def set_reflected_temp(self, value):
        """
        set value on remote device
        """
        try:
            self.make_alert(b'\x21', struct.pack('f', float(value)))
        except ValueError as ex:
            pprint.pprint(ex)

    def set_emissivity(self, value):
        """
        set value on remote device
        """
        try:
            self.make_alert(b'\x22', struct.pack('f', float(value)))
        except ValueError as ex:
            pprint.pprint(ex)

    def set_ambient_temp_shift(self, value):
        """
        set value on remote device
        """
        try:
            self.make_alert(b'\x23', struct.pack('f', float(value)))
        except ValueError as ex:
            pprint.pprint(ex)

    # def set_refresh_rate(self, value):
    #     """
    #     set value on remote device
    #     """
    #     self.make_alert(0x24, Constants.REFRESH_RATES[value])

    # def set_resolution(self, value):
    #     """
    #     set value on remote device
    #     """
    #     self.make_alert(0x25, Constants.RESOLUTIONS[value])

#CLIENT = UdpClient("192.168.1.250", 9999)
CLIENT = UdpClient("192.168.20.114", 9999)

class Constants:
    """
    class constains static data
    """

    REFRESH_RATES = {"0.5 Hz" : 0x00, "1 Hz" : 0x01, "2 Hz" : 0x02,
                     "4 Hz" : 0x03, "8 Hz" : 0x04, "16 Hz" : 0x05, "32 Hz" : 0x06, "64 Hz" : 0x07}
    RESOLUTIONS = {"16 bits" : 0x00, "17 bits" : 0x01, "18 bits" : 0x02, "19 bits" : 0x03}
    ROWS = 12
    COLS = 16
    TABLE_BOX_SIZE = 50
    APP_NAME = "Sensor monitor"
    TEMP_THRESHOLD = 35.0
    # -----------------------------------------------------------
    LABEL_SIZE = 200
    LABEL_BUTTON_SIZE = 50
    LABEL_CURRENT_VALUE = "Current value"
    LABEL_SET = "Set"
    LABEL_REFLECTED_TEMPERATURE = "Reflected Temperature"
    LABEL_EMISSIVITY = "Emissivity"
    LABEL_TEMPERATURE_AMBIENT_SHIFT = "Ambient temperature shift"
    LABEL_REFRESH_RATE = "Resfresh rate"
    LABEL_RESOLUTION = "Resolution"
    LABEL_AMBIENT_TEMPERATURE = "Tamb"
    LABEL_SUPPLY_VOLTAGE = "Vdd"
    LABEL_TABLE_TEMPERATURES = "Object temperatures"
    LABEL_TABLE_TEMPERATURES_SHIFTED = "Object temperatures (ambient shift)"
    LABEL_REFLECTEDTEMPCALC = "Trc"
    # -----------------------------------------------------------
    WIDGET_TEMPTABLE = "pixTo"
    WIDGET_TEMPTABLEAMBSHIFTED = "pixTa"
    WIDGET_VDD = "Vdd"
    WIDGET_TAMBCALC = "Tac"
    WIDGET_REFLECTEDTEMP = "Tr"
    WIDGET_EMISSIVITY = "ems"
    WIDGET_AMBIENTTEMPSHIFT = "Tas"
    WIDGET_REFRESHRATE = "rrt"
    WIDGET_RESOLUTION = "res"
    WIDGET_REFLECTEDTEMPCALC = "Trc" # not shown right now
    WIDGET_COLOR = "color"

UPDATED_WIDGETS = {
}

def regenerate_table(qtable, values):
    """
    update all values in templerature table
    """
    i = 0
    for row in range(Constants.ROWS):
        for col in range(Constants.COLS):
            temperature = values[i]
            qtable.item(row, col).setText(str(round(temperature, 1)))

            if temperature < Constants.TEMP_THRESHOLD:
                green = round(255 - (temperature * 6))
                if green < 0:
                    green = 0

                if green > 255:
                    green = 255

                blue = round(255 - (temperature * 6))
                if blue < 0:
                    blue = 0

                if blue > 255:
                    blue = 255

                qtable.item(row, col).setBackground(Qt5Gui.QColor(255, green, blue))
            else:
                qtable.item(row, col).setBackground(Qt5Gui.QColor(255, 255, 255))

            i += 1

            qtable.viewport().update()

def generate_table():
    """
    generate temperature table
    """
    table = Qt5Widgets.QTableWidget()
    table.setRowCount(Constants.ROWS)
    table.setColumnCount(Constants.COLS)

    for row in range(Constants.ROWS):
        table.setRowHeight(row, Constants.TABLE_BOX_SIZE)
        for col in range(Constants.COLS):
            table.setColumnWidth(col, Constants.TABLE_BOX_SIZE)
            twi = Qt5Widgets.QTableWidgetItem()
            twi.setTextAlignment(Qt5Core.Qt.AlignHCenter | Qt5Core.Qt.AlignVCenter)
            #twi.setFlags(Qt5Core.Qt.ItemIsEnabled)
            table.setItem(row, col, twi)
    regenerate_table(table, [1.0] * Constants.COLS * Constants.ROWS)

    return table

def generate_option_line(label, callback):
    """
    generate widgets with line edit
    """
    line_label = Qt5Widgets.QLabel(label)
    line_label.setFixedWidth(Constants.LABEL_SIZE)

    line_edit = Qt5Widgets.QLineEdit()

    set_button = Qt5Widgets.QPushButton(Constants.LABEL_SET)
    set_button.setFixedWidth(Constants.LABEL_BUTTON_SIZE)

    label_value = Qt5Widgets.QLabel(Constants.LABEL_CURRENT_VALUE)
    label_value.setFixedWidth(Constants.LABEL_SIZE)

    current_value = Qt5Widgets.QLabel("0")

    layout = Qt5Widgets.QHBoxLayout()
    layout.addWidget(line_label)
    layout.addWidget(line_edit)
    layout.addWidget(set_button)
    layout.addWidget(label_value)
    layout.addWidget(current_value)

    line = Qt5Widgets.QWidget()
    line.setLayout(layout)

    set_button.clicked.connect(lambda: callback(line_edit.text()))

    return line, current_value

def generate_gui():
    """
    methods generate whole gui
    """
    ###########################################################################

    app = Qt5Widgets.QApplication([Constants.APP_NAME])

    ###########################################################################

    line1, val_reflected_temp = generate_option_line(Constants.LABEL_REFLECTED_TEMPERATURE, \
        CLIENT.set_reflected_temp)
    UPDATED_WIDGETS[Constants.WIDGET_REFLECTEDTEMP] = val_reflected_temp

    line2, val_emmisivity = generate_option_line(Constants.LABEL_EMISSIVITY, CLIENT.set_emissivity)
    UPDATED_WIDGETS[Constants.WIDGET_EMISSIVITY] = val_emmisivity

    line3, val_ambt_shift = generate_option_line(Constants.LABEL_TEMPERATURE_AMBIENT_SHIFT, \
        CLIENT.set_ambient_temp_shift)
    UPDATED_WIDGETS[Constants.WIDGET_AMBIENTTEMPSHIFT] = val_ambt_shift

    buttons = Qt5Widgets.QWidget()
    buttons_layout = Qt5Widgets.QVBoxLayout()
    buttons_layout.addWidget(line1)
    buttons_layout.addWidget(line2)
    buttons_layout.addWidget(line3)
    buttons.setLayout(buttons_layout)

    ###########################################################################

    # label_refresh_rate = Qt5Widgets.QLabel("Set " + Constants.LABEL_REFRESH_RATE)
    # label_refresh_rate.setFixedWidth(Constants.LABEL_SIZE)
    # cb_refresh_rate = Qt5Widgets.QComboBox()
    # cb_refresh_rate.addItems(Constants.REFRESH_RATES.keys())
    # cb_refresh_rate.currentIndexChanged.connect(lambda: \
    #     CLIENT.set_refresh_rate(cb_refresh_rate.currentText()))

    # label_res = Qt5Widgets.QLabel("Set " + Constants.LABEL_RESOLUTION)
    # label_res.setFixedWidth(Constants.LABEL_SIZE)
    # cb_res = Qt5Widgets.QComboBox()
    # cb_res.addItems(Constants.RESOLUTIONS.keys())
    # cb_res.currentIndexChanged.connect(lambda: CLIENT.set_resolution(cb_res.currentText()))

    # cb_layout = Qt5Widgets.QHBoxLayout()
    # cb_layout.addWidget(label_refresh_rate)
    # cb_layout.addWidget(cb_refresh_rate)
    # cb_layout.addWidget(label_res)
    # cb_layout.addWidget(cb_res)

    # cb_widget = Qt5Widgets.QWidget()
    # cb_widget.setLayout(cb_layout)

    label_inf_ref_rate = Qt5Widgets.QLabel("Current " + Constants.LABEL_REFRESH_RATE)
    value_ref_rate = Qt5Widgets.QLabel("0")
    label_inf_ref_rate.setFixedWidth(Constants.LABEL_SIZE)
    value_ref_rate.setFixedWidth(Constants.LABEL_SIZE)

    UPDATED_WIDGETS[Constants.WIDGET_REFRESHRATE] = value_ref_rate

    label_inf_res = Qt5Widgets.QLabel("Current " + Constants.LABEL_RESOLUTION)
    value_res = Qt5Widgets.QLabel("0")
    label_inf_res.setFixedWidth(Constants.LABEL_SIZE)
    value_res.setFixedWidth(Constants.LABEL_SIZE)
    UPDATED_WIDGETS[Constants.WIDGET_RESOLUTION] = value_res

    info_layout1 = Qt5Widgets.QHBoxLayout()
    info_layout1.addWidget(label_inf_ref_rate)
    info_layout1.addWidget(value_ref_rate)
    info_layout1.addWidget(label_inf_res)
    info_layout1.addWidget(value_res)

    label_tamb = Qt5Widgets.QLabel(Constants.LABEL_AMBIENT_TEMPERATURE)
    value_tamb = Qt5Widgets.QLabel("0")
    label_tamb.setFixedWidth(4 * Constants.LABEL_SIZE / 6)
    value_tamb.setFixedWidth(4 * Constants.LABEL_SIZE / 6)
    UPDATED_WIDGETS[Constants.WIDGET_TAMBCALC] = value_tamb

    label_trc = Qt5Widgets.QLabel(Constants.LABEL_REFLECTEDTEMPCALC)
    value_trc = Qt5Widgets.QLabel("0")
    label_trc.setFixedWidth(4 * Constants.LABEL_SIZE / 6)
    value_trc.setFixedWidth(4 * Constants.LABEL_SIZE / 6)

    label_vdd = Qt5Widgets.QLabel(Constants.LABEL_SUPPLY_VOLTAGE)
    value_vdd = Qt5Widgets.QLabel("0")
    label_vdd.setFixedWidth(4 * Constants.LABEL_SIZE / 6)
    value_vdd.setFixedWidth(4 * Constants.LABEL_SIZE / 6)
    UPDATED_WIDGETS[Constants.WIDGET_VDD] = value_vdd

    UPDATED_WIDGETS[Constants.WIDGET_REFLECTEDTEMPCALC] = value_trc

    info_widget1 = Qt5Widgets.QWidget()
    info_widget1.setLayout(info_layout1)

    info_layout2 = Qt5Widgets.QHBoxLayout()
    info_layout2.addWidget(label_tamb)
    info_layout2.addWidget(value_tamb)
    info_layout2.addWidget(label_vdd)
    info_layout2.addWidget(value_vdd)
    info_layout2.addWidget(label_trc)
    info_layout2.addWidget(value_trc)

    info_widget2 = Qt5Widgets.QWidget()
    info_widget2.setLayout(info_layout2)

    util_layout = Qt5Widgets.QVBoxLayout()
    # util_layout.addWidget(cb_widget)
    util_layout.addWidget(info_widget1)
    util_layout.addWidget(info_widget2)

    util_widget = Qt5Widgets.QWidget()
    util_widget.setLayout(util_layout)

    setup_layout = Qt5Widgets.QHBoxLayout()
    setup_layout.addWidget(buttons)
    setup_layout.addWidget(util_widget)

    setup = Qt5Widgets.QWidget()
    setup.setLayout(setup_layout)

    ###########################################################################

    value_temp_table = generate_table()
    UPDATED_WIDGETS[Constants.WIDGET_TEMPTABLE] = value_temp_table

    value_temp_table_tamb = generate_table()
    UPDATED_WIDGETS[Constants.WIDGET_TEMPTABLEAMBSHIFTED] = value_temp_table_tamb

    tables_layout = Qt5Widgets.QHBoxLayout()
    tables_layout.addWidget(value_temp_table)
    tables_layout.addWidget(value_temp_table_tamb)
    tables_widget = Qt5Widgets.QWidget()
    tables_widget.setLayout(tables_layout)

    headers_layout = Qt5Widgets.QHBoxLayout()
    headers_layout.addWidget(Qt5Widgets.QLabel(Constants.LABEL_TABLE_TEMPERATURES))
    headers_layout.addWidget(Qt5Widgets.QLabel(Constants.LABEL_TABLE_TEMPERATURES_SHIFTED))
    headers_widget = Qt5Widgets.QWidget()
    headers_widget.setLayout(headers_layout)

    layout_window = Qt5Widgets.QVBoxLayout()
    layout_window.addWidget(headers_widget)
    layout_window.addWidget(tables_widget)
    layout_window.addWidget(setup)

    widget_window = Qt5Widgets.QWidget()
    widget_window.setLayout(layout_window)
    widget_window.showMaximized()

    CLIENT.start()
    app.exec_()
    CLIENT.stop()
    CLIENT.join()

def main():
    """
    main method
    """
    generate_gui()

if __name__ == "__main__":
    main()
